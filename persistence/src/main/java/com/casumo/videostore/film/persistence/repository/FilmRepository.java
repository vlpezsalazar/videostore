package com.casumo.videostore.film.persistence.repository;

import com.casumo.videostore.film.dao.FilmDao;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.persistence.entity.FilmEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional(readOnly = true)
public interface FilmRepository extends CrudRepository<FilmEntity, Long>, FilmDao {

    @Transactional
    @Override
    default FilmModel persist(FilmModel filmModel) {
        return save(FilmEntity.from(filmModel));
    }

    @Override
    default Optional<FilmModel> findById(Long id){
        return Optional.ofNullable(findOne(id));
    }
}
