package com.casumo.videostore.film.persistence.entity;

import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.model.FilmType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Film")
public class FilmEntity implements FilmModel {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String filmName;

    @Enumerated(EnumType.STRING)
    private FilmType filmType;

    public void setId(Long id) {
        this.id = id;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public void setFilmType(FilmType filmType) {
        this.filmType = filmType;
    }

    @Override
    public long id() {
        return id;
    }

    @Override
    public String filmName() {
        return filmName;
    }

    @Override
    public FilmType filmType() {
        return filmType;
    }


    public static FilmEntity from(FilmModel filmModel){
        if (filmModel instanceof FilmEntity) return (FilmEntity) filmModel;
        final FilmEntity filmEntity = new FilmEntity();
        filmEntity.setId(filmModel.id());
        filmEntity.setFilmName(filmModel.filmName());
        filmEntity.setFilmType(filmModel.filmType());
        return filmEntity;
    }
}
