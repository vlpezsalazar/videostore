package com.casumo.videostore.customer.persistence.entity;

import com.casumo.videostore.customer.model.CustomerModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Customer")
public class CustomerEntity implements CustomerModel {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(nullable = false)
    private String name;

    private long earnedBonusPoints;


    @Override
    public long id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public long earnedBonusPoints() {
        return earnedBonusPoints;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEarnedBonusPoints(long earnedBonusPoints) {
        this.earnedBonusPoints = earnedBonusPoints;
    }

    public static CustomerEntity from(CustomerModel cm) {
        if (cm instanceof CustomerEntity) return (CustomerEntity) cm;
        final CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(cm.id());
        customerEntity.setName(cm.name());
        customerEntity.setEarnedBonusPoints(cm.earnedBonusPoints());

        return customerEntity;

    }
}
