package com.casumo.videostore.customer.persistence.repository;

import com.casumo.videostore.customer.dao.CustomerDao;
import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.persistence.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long>, CustomerDao {

    @Transactional
    Long deleteById(Long id);

    @Override
    @Transactional
    default CustomerModel persist(CustomerModel customerInfo){
        return this.save(CustomerEntity.from(customerInfo));
    }

}
