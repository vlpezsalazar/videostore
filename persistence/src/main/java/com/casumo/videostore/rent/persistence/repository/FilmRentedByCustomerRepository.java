package com.casumo.videostore.rent.persistence.repository;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rent.persistence.entity.FilmRentedByCustomerEntity;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(readOnly = true)
public interface FilmRentedByCustomerRepository extends JpaRepository<FilmRentedByCustomerEntity, Long>, FilmRentedByCustomerDao {

    @Transactional
    @Override
    default FilmRentedByCustomerModel persist(FilmRentedByCustomerModel filmRentedByCustomerModel){

        return this.saveAndFlush(FilmRentedByCustomerEntity.from(filmRentedByCustomerModel));
    }

    default List<FilmRentedByCustomerModel> findLastFilmRentalNotYetReturned(CustomerModel customerModel, FilmModel filmModel){
        return toModel(findFirst1ByCustomerEntityIdAndFilmEntityIdAndReturnedOnNullOrderByRentedOnDesc(customerModel.id(), filmModel.id()));
    }

    default List<FilmRentedByCustomerModel> findByFilmModelOrderByRentedOnDesc(FilmModel filmModel, int pageNumber) {
        return toModel(findByFilmEntityIdOrderByRentedOnDesc(filmModel.id(), new PageRequest(pageNumber,10, Sort.Direction.DESC, "rentedOn")));
    }
    default List<FilmRentedByCustomerModel> findAllFilmRentalNotYetReturned(CustomerModel customer){
        return toModel(findByCustomerEntityIdAndReturnedOnNullOrderByRentedOnDesc(customer.id()));
    }


    default List<FilmRentedByCustomerModel> toModel(List<FilmRentedByCustomerEntity> films) {
        return new ArrayList<>(films);
    }

    List<FilmRentedByCustomerEntity> findFirst1ByCustomerEntityIdAndFilmEntityIdAndReturnedOnNullOrderByRentedOnDesc(Long customerId, Long filmId);

    List<FilmRentedByCustomerEntity> findByFilmEntityIdOrderByRentedOnDesc(long filmEntity, Pageable pageable);

    List<FilmRentedByCustomerEntity> findByCustomerEntityIdAndReturnedOnNullOrderByRentedOnDesc(long id);

}
