package com.casumo.videostore.rent.persistence.entity;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.persistence.entity.CustomerEntity;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.persistence.entity.FilmEntity;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "Rental")
public class FilmRentedByCustomerEntity implements FilmRentedByCustomerModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private long id;

    @OneToOne
    @JoinColumn(nullable=false, updatable=false)
    private FilmEntity filmEntity;

    @OneToOne
    @JoinColumn(nullable=false, updatable=false)
    private CustomerEntity customerEntity;

    private int daysRented;
    private double toPay;
    private double paid;
    @Column(nullable = false)
    private Date rentedOn;

    private Date returnedOn;


    @Override
    public Optional<Long> id() {
        return Optional.of(id);
    }

    @Override
    public FilmModel filmModel() {
        return filmEntity;
    }

    @Override
    public CustomerModel customerModel() {
        return customerEntity;
    }

    @Override
    public int daysRented() {
        return daysRented;
    }

    @Override
    public double toPay() {
        return toPay;
    }

    @Override
    public double paid() {
        return paid;
    }

    @Override
    public Date rentedOn() {
        return rentedOn;
    }

    @Override
    public Optional<Date> returnedOn() {
        return Optional.ofNullable(returnedOn);
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFilmEntity(FilmEntity filmEntity) {
        this.filmEntity = filmEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    public void setDaysRented(int daysRented) {
        this.daysRented = daysRented;
    }

    public void setToPay(double toPay) {
        this.toPay = toPay;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public void setRentedOn(Date rentedOn) {
        this.rentedOn = rentedOn;
    }

    public void setReturnedOn(Date returnedOn) {
        this.returnedOn = returnedOn;
    }

    public static FilmRentedByCustomerEntity from(FilmRentedByCustomerModel draft) {
        if (draft instanceof FilmRentedByCustomerEntity) return (FilmRentedByCustomerEntity) draft;
        final FilmRentedByCustomerEntity filmRentedByCustomerEntity = new FilmRentedByCustomerEntity();
        if (draft.id().isPresent()) filmRentedByCustomerEntity.setId(draft.id().get());
        filmRentedByCustomerEntity.setFilmEntity((FilmEntity) draft.filmModel());
        filmRentedByCustomerEntity.setCustomerEntity((CustomerEntity) draft.customerModel());
        filmRentedByCustomerEntity.setDaysRented(draft.daysRented());
        filmRentedByCustomerEntity.setPaid(draft.paid());
        filmRentedByCustomerEntity.setToPay(draft.toPay());
        filmRentedByCustomerEntity.setRentedOn(draft.rentedOn());
        filmRentedByCustomerEntity.setReturnedOn(draft.returnedOn().orElse(null));
        return filmRentedByCustomerEntity;
    }
}

