USE videostore;

ALTER TABLE CustomerEntity RENAME TO Customer;
ALTER TABLE FilmEntity RENAME TO Film;
ALTER TABLE FilmRentedByCustomerEntity RENAME TO Rental;
