CREATE SCHEMA IF NOT EXISTS videostore;

USE videostore;

CREATE TABLE IF NOT EXISTS CustomerEntity
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    earnedBonusPoints BIGINT(10) NOT NULL,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS FilmEntity
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    filmName VARCHAR(255) NOT NULL,
    filmType VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS FilmRentedByCustomerEntity
(
    id BIGINT(20) PRIMARY KEY NOT NULL,
    filmEntityId BIGINT(20) NOT NULL,
    customerEntityId BIGINT(20) NOT NULL,
    daysRented INT NOT NULL,
    toPay DOUBLE,
    paid DOUBLE NOT NULL,
    rentedOn DATETIME NOT NULL,
    returnedOn DATETIME
);
