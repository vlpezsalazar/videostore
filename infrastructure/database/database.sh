#!/usr/bin/env bash

set -e

startDatabase () {

    local db_schema=$1
    local db_user=$2
    local db_password=$3

    local container=$(docker ps -af name=database --format {{.Status}})

    echo "Running with ${db_schema} ${db_user} ${db_password}"
    echo "$(docker ps -af name=database)"

    case $container in
    *Up*)
        echo "Container is already running."
    ;;

    *Exited*|*Stopped*)
        echo "Starting db container"
        docker start database
    ;;

    *)
    echo "Creating db container: docker run --name database -e \"MYSQL_ROOT_PASSWORD=password\"  -e \"MYSQL_DATABASE=${db_schema}\" -e \"MYSQL_USER=${db_user}\" \ -e \"MYSQL_PASSWORD=${db_password}\" \ -d \"mysql\""

    docker run --name database \
               -p 3306:3306 \
               -e "MYSQL_ROOT_PASSWORD=password" \
               -e "MYSQL_DATABASE=${db_schema}" \
               -e "MYSQL_USER=${db_user}" \
               -e "MYSQL_PASSWORD=${db_password}" \
               -d "mysql"
    esac

    docker run --health-cmd='mysqladmin ping --silent' -d mysql
}

if [ "$#" -eq 3 ] ; then
    startDatabase $1 $2 $3
else
    echo "You should provide <db_schema> <db_user> <db_password>"
fi