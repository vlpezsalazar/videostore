#!/usr/bin/env bash

set -e

startSwagger () {

    local container=$(docker ps -af name=swagger --format {{.Status}})

    echo "Running Swagger UI..."
    echo "$(docker ps -af name=swagger)"

    case $container in
    *Up*)
        echo "Container is already running."
    ;;

    *Exited*|*Stopped*)
        echo "Starting swagger container"
        docker start swagger
    ;;

    *)
    echo "Creating swagger container: docker run --name swagger -p 80:8080 -e \"API_URL=http://localhost:8080/videostore/rest/v0/swagger.json}\" \ -d \"swaggerapi/swagger-ui\""

    docker run --name swagger \
               -p 80:8080 \
               -e "API_URL=http://localhost:8080/videostore/rest/v0/swagger.json" \
               -d "swaggerapi/swagger-ui"
    esac

}

startSwagger
