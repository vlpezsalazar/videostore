package com.casumo.videostore.film.model;

import com.casumo.videostore.rental.model.PriceModel;

public enum FilmType implements PriceModel{
    NEW_RELEASE(40, 2) {
        @Override
        public double price(int days) {
            return this.regularPrice()*days;
        }
    },
    REGULAR_FILM(30, 1){
        @Override
        public double price(int days){
            return this.priceFormulae(days, 3);
        }
    },
    OLD_FILM(30, 1) {
        @Override
        public double price(int days) {
            return this.priceFormulae(days, 5);
        }
    };

    private final double regularPrice;
    private final long points;

    FilmType(double regularPrice, long points) {
        this.regularPrice = regularPrice;
        this.points = points;
    }

    @Override
    public double regularPrice() {
        return regularPrice;
    }

    protected  double priceFormulae(int days, int normalPriceDays) {
        final double regularPrice = this.regularPrice();
        return regularPrice + regularPrice * Math.max(0, days-normalPriceDays);
    }

    public long points() {
        return points;
    }
}
