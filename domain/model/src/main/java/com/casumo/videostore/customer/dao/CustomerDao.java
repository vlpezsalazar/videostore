package com.casumo.videostore.customer.dao;

import com.casumo.videostore.customer.model.CustomerModel;

import java.util.Optional;

public interface CustomerDao {
    Optional<CustomerModel> findById(Long id);

    CustomerModel persist(CustomerModel customerInfo);

    Long deleteById(Long id);
}
