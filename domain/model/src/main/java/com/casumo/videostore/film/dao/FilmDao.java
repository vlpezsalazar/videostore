package com.casumo.videostore.film.dao;

import com.casumo.videostore.film.model.FilmModel;

import java.util.Optional;

public interface FilmDao {
    FilmModel persist(FilmModel filmModel);

    Optional<FilmModel> findById(Long id);

    Long deleteById(Long id);
}
