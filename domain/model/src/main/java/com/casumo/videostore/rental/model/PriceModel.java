package com.casumo.videostore.rental.model;


public interface PriceModel {
    double regularPrice();

    double price(int days);
}
