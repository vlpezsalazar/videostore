package com.casumo.videostore.rental.dao;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;

import java.util.List;

public interface FilmRentedByCustomerDao {
    FilmRentedByCustomerModel persist(FilmRentedByCustomerModel filmRentedByCustomerModel);

    List<FilmRentedByCustomerModel> findLastFilmRentalNotYetReturned(CustomerModel customerModel, FilmModel filmModel);

    List<FilmRentedByCustomerModel> findByFilmModelOrderByRentedOnDesc(FilmModel filmModel, int pageNumber);

    List<FilmRentedByCustomerModel> findAllFilmRentalNotYetReturned(CustomerModel customer);
}
