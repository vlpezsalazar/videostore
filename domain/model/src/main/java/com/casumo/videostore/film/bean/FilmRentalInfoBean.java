package com.casumo.videostore.film.bean;

import com.casumo.videostore.film.model.FilmModel;
import org.immutables.value.Value;

import java.time.Instant;
import java.util.Date;


@Value.Style(allParameters = true, of = "new", visibility = Value.Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Value.Immutable
public interface FilmRentalInfoBean {
    FilmModel film();

    int daysToRent();

    double price();

    @Value.Default
    default Date availableOn(){
        return Date.from(Instant.now());
    }

    static Builder builder() {
        return new Builder();
    }


    class Builder extends ImmutableFilmRentalInfoBean.Builder{}

}
