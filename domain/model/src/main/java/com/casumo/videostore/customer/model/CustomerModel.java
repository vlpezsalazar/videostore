package com.casumo.videostore.customer.model;

import static org.immutables.value.Value.Default;
import static org.immutables.value.Value.Immutable;
import static org.immutables.value.Value.Parameter;
import static org.immutables.value.Value.Style;

@Style(of = "new", visibility = Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Immutable
public interface CustomerModel {
    @Default
    default long id(){
        return 0;
    }

    @Parameter
    String name();

    @Default
    default long earnedBonusPoints(){
        return 0;
    }

    static Builder builder(){
        return new Builder();
    }

    class Builder extends ImmutableCustomerModel.Builder {}
}
