package com.casumo.videostore.film.model;

import static org.immutables.value.Value.Default;
import static org.immutables.value.Value.Immutable;
import static org.immutables.value.Value.Parameter;
import static org.immutables.value.Value.Style;

@Style(of = "new", visibility = Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Immutable
public interface FilmModel {
    @Default
    default long id() {
        return 0;
    }

    @Parameter
    String filmName();

    @Default
    default FilmType filmType() {
        return FilmType.REGULAR_FILM;
    }

    static Builder builder() {
        return new Builder();
    }

    class Builder extends ImmutableFilmModel.Builder {
    }
}
