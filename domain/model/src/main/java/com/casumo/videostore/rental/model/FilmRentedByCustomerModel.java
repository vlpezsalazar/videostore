package com.casumo.videostore.rental.model;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.model.FilmModel;
import org.immutables.value.Value;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Value.Style(allParameters = true, of = "new", visibility = Value.Style.ImplementationVisibility.PACKAGE, overshadowImplementation = true)
@Value.Immutable
public interface FilmRentedByCustomerModel {
    Optional<Long> id();
    FilmModel filmModel();
    CustomerModel customerModel();
    int daysRented();
    @Value.Default
    default double toPay(){ return 0;}
    double paid();

    Date rentedOn();

    Optional<Date> returnedOn();

    static Builder builder(){
        return new Builder();
    }

    default boolean isRentalPeriodLessThanToday(){
        return rentedOn().toInstant().plus(daysRented(), DAYS).isBefore(Instant.now());
    }

    class Builder extends ImmutableFilmRentedByCustomerModel.Builder {}

}
