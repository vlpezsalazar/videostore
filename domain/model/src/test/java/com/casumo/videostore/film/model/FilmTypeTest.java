package com.casumo.videostore.film.model;

import org.junit.Test;

import static com.casumo.videostore.film.model.FilmType.NEW_RELEASE;
import static com.casumo.videostore.film.model.FilmType.OLD_FILM;
import static com.casumo.videostore.film.model.FilmType.REGULAR_FILM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class FilmTypeTest {

    @Test
    public void price_NEW_RELEASE_shouldReturnPremiumPriceTimesDays() {
        //given
        int days = 4;
        final double regularPrice = NEW_RELEASE.regularPrice();

        //when
        final double price = NEW_RELEASE.price(days);

        //then
        assertThat(40d, is(regularPrice));
        assertThat(price, is(days * regularPrice));

    }

    @Test
    public void price_REGULAR_FILM_shouldReturnBasicPrice_whenDaysAreLessOrEqualThan3() {
        //given
        int days = 3;
        final double regularPrice = REGULAR_FILM.regularPrice();

        //when
        final double price = REGULAR_FILM.price(days);

        //then
        assertThat(30d, is(regularPrice));
        assertThat(price, is(regularPrice));
    }

    @Test
    public void price_REGULAR_FILM_shouldReturnBasicPricePlusBasicPriceTimesDaysOver3_whenDaysAreOver3() {
        //given
        int days = 5;
        final double regularPrice = REGULAR_FILM.regularPrice();

        //when
        final double price = REGULAR_FILM.price(days);

        //then
        assertThat(price, is(regularPrice + regularPrice * (days - 3)));
    }

    @Test
    public void price_OLD_FILM_shouldReturnBasicPrice_whenDaysAreLessOrEqualThan5() {
        //given
        int days = 5;
        final double regularPrice = REGULAR_FILM.regularPrice();

        //when
        final double price = OLD_FILM.price(days);

        //then
        assertThat(30d, is(regularPrice));
        assertThat(price, is(regularPrice));
    }

    @Test
    public void price_OLD_FILM_shouldReturnBasicPricePlusBasicPriceTimesDaysOver5_whenDaysAreLessOrEqualThan5() {
        //given
        int days = 7;

        //when
        final double price = OLD_FILM.price(days);

        //then
        assertThat(price, is(30d + 30 * (days - 5)));
    }

}