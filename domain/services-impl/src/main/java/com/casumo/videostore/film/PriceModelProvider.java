package com.casumo.videostore.film;

import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.model.PriceModel;

public interface PriceModelProvider {
    PriceModel getPriceModel(FilmModel film);
}
