package com.casumo.videostore.rental;

import com.casumo.videostore.customer.dao.CustomerDao;
import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.FilmInfoService;
import com.casumo.videostore.film.PriceModelProvider;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.exception.AlreadyRentedException;
import com.casumo.videostore.rental.exception.FilmWasNotRentedByCustomerException;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;

import javax.inject.Named;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Named
public class CustomerRentalServiceImpl implements CustomerRentalService {

    private final FilmInfoService filmInfoService;
    private final CustomerDao customerDao;
    private final FilmRentedByCustomerDao filmRentedByCustomerDao;
    private final PriceModelProvider priceModelProvider;

    public CustomerRentalServiceImpl(PriceModelProvider priceModelProvider, FilmInfoService filmInfoService, CustomerDao customerDao, FilmRentedByCustomerDao filmRentedByCustomerDao) {
        this.priceModelProvider = priceModelProvider;
        this.filmInfoService = filmInfoService;
        this.customerDao = customerDao;
        this.filmRentedByCustomerDao = filmRentedByCustomerDao;
    }

    @Override
    public Optional<FilmRentedByCustomerModel> rentalInfo(CustomerModel customer, FilmModel film) {
        final List<FilmRentedByCustomerModel> filmsRentedBy = filmRentedByCustomerDao.findLastFilmRentalNotYetReturned(customer, film);

        if (filmsRentedBy.isEmpty()) {
            return Optional.empty();
        }

        FilmRentedByCustomerModel filmRentedByCustomer = filmsRentedBy.get(0);
        if (filmRentedByCustomer.isRentalPeriodLessThanToday()) {
            filmRentedByCustomer = FilmRentedByCustomerModel
                    .builder()
                    .from(filmRentedByCustomer)
                    .toPay(getTotalRentalPriceToday(filmRentedByCustomer) - filmRentedByCustomer.toPay())
                    .build();
        }

        return Optional.of(filmRentedByCustomer);
    }

    private double getTotalRentalPriceToday(FilmRentedByCustomerModel filmRentedByCustomerModel) {
        final int days = Math.toIntExact((Instant.now().getEpochSecond() - filmRentedByCustomerModel.rentedOn().toInstant().getEpochSecond()) / 86400);
        return filmInfoService.filmRentalInfo(filmRentedByCustomerModel.filmModel(), days).price();
    }

    @Override
    public FilmRentedByCustomerModel rentFilm(CustomerModel customer, FilmModel film, int days) {
        if (isFilmAlreadyRented(film)) {
            throw new AlreadyRentedException(customer, film);
        }


        final CustomerModel updatedCustomer = customerDao.persist(CustomerModel.builder().from(customer)
                .earnedBonusPoints(customer.earnedBonusPoints() + calulatePointsGivenBy(film))
                .build());

        final FilmRentedByCustomerModel filmRentedByCustomer = FilmRentedByCustomerModel
                .builder()
                .customerModel(updatedCustomer)
                .filmModel(film)
                .paid(filmInfoService.filmRentalInfo(film, days).price())
                .rentedOn(Date.from(Instant.now()))
                .daysRented(days)
                .build();

        filmRentedByCustomerDao.persist(filmRentedByCustomer);
        return filmRentedByCustomer;
    }

    private long calulatePointsGivenBy(FilmModel film) {
        return film.filmType().points();
    }

    @Override
    public FilmRentedByCustomerModel returnFilm(CustomerModel customer, FilmModel film) {
        final List<FilmRentedByCustomerModel> filmsRentedBy = filmRentedByCustomerDao.findLastFilmRentalNotYetReturned(customer, film);

        if (filmsRentedBy.isEmpty()) {
            throw new FilmWasNotRentedByCustomerException(customer, film);
        }

        final FilmRentedByCustomerModel filmRentedByCustomer = filmsRentedBy.get(0);

        final FilmRentedByCustomerModel returnedFilm = FilmRentedByCustomerModel
                .builder()
                .from(filmRentedByCustomer)
                .paid(getTotalRentalPriceToday(filmRentedByCustomer))
                .toPay(0)
                .returnedOn(Date.from(Instant.now()))
                .build();

        filmRentedByCustomerDao.persist(returnedFilm);

        return returnedFilm;

    }

    private boolean isFilmAlreadyRented(FilmModel film) {
        final Date availabilityDate = filmInfoService.filmAvailabilityDate(film);
        return availabilityDate.after(Date.from(Instant.now()));
    }

}
