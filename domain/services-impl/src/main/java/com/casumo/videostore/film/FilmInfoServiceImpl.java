package com.casumo.videostore.film;

import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.model.PriceModel;

import javax.inject.Named;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.DAYS;

@Named
public class FilmInfoServiceImpl implements FilmInfoService {

    private final FilmRentedByCustomerDao filmRentedByCustomerDao;
    private final PriceModelProvider priceModelProvider;

    public FilmInfoServiceImpl(FilmRentedByCustomerDao filmRentedByCustomerDao, PriceModelProvider priceModelProvider) {
        this.filmRentedByCustomerDao = filmRentedByCustomerDao;
        this.priceModelProvider = priceModelProvider;
    }

    @Override
    public FilmRentalInfoBean filmRentalInfo(FilmModel filmModel, int days) {

        final PriceModel priceModel = priceModelProvider.getPriceModel(filmModel);

        return FilmRentalInfoBean.builder()
                .film(filmModel)
                .daysToRent(days)
                .price(priceModel.price(days))
                .availableOn(filmAvailabilityDate(filmModel))
                .build();
    }

    @Override
    public Date filmAvailabilityDate(FilmModel filmModel) {
        final List<FilmRentedByCustomerModel> lastRentalsOfFilm = filmRentedByCustomerDao.findByFilmModelOrderByRentedOnDesc(filmModel, 0);
        if (!lastRentalsOfFilm.isEmpty()){
            final FilmRentedByCustomerModel filmRentedByCustomerModel = lastRentalsOfFilm.get(0);
            final Optional<Date> returnedOn = filmRentedByCustomerModel.returnedOn();
            if (!returnedOn.isPresent()){
                final long daysRented = filmRentedByCustomerModel.daysRented();
                final Date returnDate = Date.from(filmRentedByCustomerModel.rentedOn().toInstant().plus(daysRented, DAYS));
                return returnDate;
            }
        }

        return Date.from(Instant.now());
    }
}
