package com.casumo.videostore.film;

import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.model.PriceModel;

import javax.inject.Named;

@Named
public class PriceModelProviderImpl implements PriceModelProvider {

    @Override
    public PriceModel getPriceModel(FilmModel film){
        return film.filmType();
    }
}
