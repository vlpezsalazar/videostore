package com.casumo.videostore.film;

import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.domain.test.fixtures.FilmRentedByCustomerModelFixtures;
import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static java.time.temporal.ChronoUnit.DAYS;
import static org.exparity.hamcrest.date.DateMatchers.within;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;

public class FilmInfoServiceImplTest {


    @Mock
    private FilmRentedByCustomerDao filmRentedByCustomerDao;

    private FilmInfoService filmInfoService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.filmInfoService = new FilmInfoServiceImpl(filmRentedByCustomerDao, new PriceModelProviderImpl());
    }

    @Test
    public void filmRentalInfo_shouldCalculateThePriceForRentingAFilmAndSetTheAvailabilityDateToToday_whenTheFilmExistsAndIsNotRented() throws Exception {
        //given
        int daysToRent = 4;
        final FilmModel avengers = FilmModelFixtures.avengers();
        given(filmRentedByCustomerDao.findByFilmModelOrderByRentedOnDesc(avengers, 1))
                .willReturn(Collections.emptyList());

        //when
        final FilmRentalInfoBean filmRentalInfoBean = filmInfoService.filmRentalInfo(avengers, daysToRent);

        //then
        assertThat(filmRentalInfoBean.daysToRent(), is(daysToRent));
        assertThat(filmRentalInfoBean.film(), sameBeanAs(avengers));
        assertThat(filmRentalInfoBean.availableOn(), within(1, ChronoUnit.SECONDS, Date.from(Instant.now())));
        assertThat(filmRentalInfoBean.price(), is(avengers.filmType().price(daysToRent)));
    }

    @Test
    public void filmRentalInfo_shouldCalculateThePriceForRentingAFilmAndSetTheAvailabilityDateToToday_whenTheFilmExistsIsRentedAndReturned() throws Exception {
        //given
        int daysToRent = 4;
        final FilmModel avengers = FilmModelFixtures.avengers();
        final FilmRentedByCustomerModel avengersLastRental = FilmRentedByCustomerModel.builder()
                .from(FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengersTodayFor2DaysPaying100())
                .returnedOn(Date.from(Instant.now()))
                .build();
        given(filmRentedByCustomerDao.findByFilmModelOrderByRentedOnDesc(avengers, 1))
                .willReturn(Collections.singletonList(avengersLastRental));

        //when
        final FilmRentalInfoBean filmRentalInfoBean = filmInfoService.filmRentalInfo(avengers, daysToRent);

        //then
        assertThat(filmRentalInfoBean.daysToRent(), is(daysToRent));
        assertThat(filmRentalInfoBean.film(), sameBeanAs(avengers));
        assertThat(filmRentalInfoBean.availableOn(), within(1, ChronoUnit.SECONDS, Date.from(Instant.now())));
        assertThat(filmRentalInfoBean.price(), is(avengers.filmType().price(daysToRent)));
    }

    @Test
    public void filmRentalInfo_shouldCalculateThePriceForRentingAFilmAndSetTheAvailabilityDateToTheDateToReturn_whenTheFilmExistsIsRentedAndNotReturned() throws Exception {
        //given
        int daysToRent = 4;
        final FilmModel avengers = FilmModelFixtures.avengers();
        given(filmRentedByCustomerDao.findByFilmModelOrderByRentedOnDesc(avengers, 0))
                .willReturn(Collections.singletonList(FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengersTodayFor2DaysPaying100()));

        //when
        final FilmRentalInfoBean filmRentalInfoBean = filmInfoService.filmRentalInfo(avengers, daysToRent);

        //then
        assertThat(filmRentalInfoBean.daysToRent(), is(daysToRent));
        assertThat(filmRentalInfoBean.film(), sameBeanAs(avengers));
        assertThat(filmRentalInfoBean.availableOn(), within(1, ChronoUnit.SECONDS, Date.from(Instant.now().plus(2, DAYS))));
        assertThat(filmRentalInfoBean.price(), is(avengers.filmType().price(daysToRent)));
    }



}