package com.casumo.videostore.rental;

import com.casumo.videostore.customer.dao.CustomerDao;
import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.domain.test.AssertException;
import com.casumo.videostore.domain.test.IAssertException;
import com.casumo.videostore.domain.test.fixtures.CustomerModelFixtures;
import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.domain.test.fixtures.FilmRentalInfoBeanFixtures;
import com.casumo.videostore.domain.test.fixtures.FilmRentedByCustomerModelFixtures;
import com.casumo.videostore.film.FilmInfoService;
import com.casumo.videostore.film.PriceModelProviderImpl;
import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.exception.AlreadyRentedException;
import com.casumo.videostore.rental.exception.FilmWasNotRentedByCustomerException;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.exparity.hamcrest.date.DateMatchers.within;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class CustomerRentalServiceImplTest {

    @Mock
    private FilmRentedByCustomerDao filmRentedByCustomerDao;

    @Mock
    private FilmInfoService filmInfoService;

    @Mock
    private CustomerDao customerDao;

    @Captor
    private ArgumentCaptor<CustomerModel> captor;

    private CustomerRentalServiceImpl customerRentalService;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        customerRentalService = new CustomerRentalServiceImpl(new PriceModelProviderImpl(), filmInfoService, customerDao, filmRentedByCustomerDao);
    }

    @Test
    public void rentFilm_shouldCreateAFilmRentedByCustomerModel_whenTheFilmIsAvailableForRent() throws Exception {
        //given
        final int daysToRent = 4;
        final CustomerModel customer = CustomerModelFixtures.goodCustomer();
        final FilmModel film = FilmModelFixtures.avengers();
        final FilmRentalInfoBean filmRentalInfoBean = FilmRentalInfoBeanFixtures.avengersRentalFor2Days();
        given(customerDao.persist(any())).willAnswer(x -> x.getArgument(0));
        given(filmInfoService.filmAvailabilityDate(film)).willReturn(Date.from(Instant.now()));
        given(filmInfoService.filmRentalInfo(film, daysToRent)).willReturn(filmRentalInfoBean);

        //when
        final FilmRentedByCustomerModel filmRentedByCustomerModel = customerRentalService.rentFilm(customer, film, daysToRent);

        //then
        verify(customerDao).persist(captor.capture());
        verify(filmRentedByCustomerDao).persist(filmRentedByCustomerModel);
        assertThat(filmRentedByCustomerModel.filmModel(), is(film));
        final CustomerModel value = captor.getValue();
        assertThat(value.earnedBonusPoints(), is(customer.earnedBonusPoints() + 1));
        assertThat(filmRentedByCustomerModel.customerModel(), is(value));
        assertThat(filmRentedByCustomerModel.daysRented(), is(daysToRent));
        assertThat(filmRentedByCustomerModel.rentedOn(), within(1, ChronoUnit.SECONDS, Date.from(Instant.now())));
        assertThat(filmRentedByCustomerModel.returnedOn().isPresent(), is(false));
        assertThat(filmRentedByCustomerModel.paid(), is(filmRentalInfoBean.price()));
        assertThat(filmRentedByCustomerModel.toPay(), is(0d));
    }

    @Test
    public void rentFilm_shouldThrowFilmAlreadyRentedException_whenTheFilmIsWasAvailableForRent() throws Exception {
        //given
        final int daysToRent = 4;
        final CustomerModel customer = CustomerModelFixtures.goodCustomer();
        final FilmModel film = FilmModelFixtures.avengers();
        given(filmInfoService.filmAvailabilityDate(film)).willReturn(Date.from(Instant.now().plus(1, DAYS)));

        //when
        IAssertException assertExceptionThrown = AssertException.captureException(() -> customerRentalService.rentFilm(customer, film, daysToRent));

        //then
        assertExceptionThrown.is(AlreadyRentedException.class);
        verifyNoMoreInteractions(filmRentedByCustomerDao);
    }


    @Test
    public void returnFilm_shouldUpdateTheFilmRentedByThenCustomer_whenTheRentalExists() throws Exception {
        //given
        final FilmRentedByCustomerModel filmsRentedByCustomer = FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengers4DaysAgoFor2DaysPayingBasicPrice();
        final CustomerModel customer = filmsRentedByCustomer.customerModel();
        final FilmModel film = filmsRentedByCustomer.filmModel();
        given(filmRentedByCustomerDao.findLastFilmRentalNotYetReturned(eq(customer), any())).willReturn(Collections.singletonList(filmsRentedByCustomer));
        given(filmInfoService.filmRentalInfo(eq(film), eq(4))).willReturn(FilmRentalInfoBeanFixtures.avengersRentalFor4Days());

        //when
        final FilmRentedByCustomerModel filmRentedByCustomerModel = customerRentalService.returnFilm(customer, film);

        //then
        verify(filmRentedByCustomerDao).persist(filmRentedByCustomerModel);
        assertThat(filmRentedByCustomerModel.filmModel(), is(film));
        assertThat(filmRentedByCustomerModel.customerModel(), is(customer));
        assertThat(filmRentedByCustomerModel.daysRented(), is(2));
        assertThat(filmRentedByCustomerModel.rentedOn(), is(filmsRentedByCustomer.rentedOn()));
        assertThat(filmRentedByCustomerModel.returnedOn().get(), within(1, ChronoUnit.SECONDS, Date.from(Instant.now())));
        assertThat(filmRentedByCustomerModel.paid(), is(60d));
        assertThat(filmRentedByCustomerModel.toPay(), is(0d));
    }

    @Test
    public void returnFilm_shouldThrowException_whenTheFilmIsWasAvailableForRent() throws Exception {
        //given
        final CustomerModel customer = CustomerModelFixtures.goodCustomer();
        final FilmModel film = FilmModelFixtures.avengers();
        given(filmInfoService.filmAvailabilityDate(film)).willReturn(Date.from(Instant.now().plus(1, DAYS)));
        given(filmRentedByCustomerDao.findLastFilmRentalNotYetReturned(eq(customer), any())).willReturn(Collections.emptyList());

        //when
        IAssertException assertExceptionThrown = AssertException.captureException(() -> customerRentalService.returnFilm(customer, film));

        //then
        assertExceptionThrown.is(FilmWasNotRentedByCustomerException.class);
    }


}