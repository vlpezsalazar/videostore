package com.casumo.videostore.film;

import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.model.FilmModel;

import java.util.Date;

public interface FilmInfoService {

    /**
     * Computes the price to rent the film for a number of days and when it will be available.
     * Returns this information in the response.
     * @param filmModel the id of the film.
     * @param days number of days to
     * @return the rental information containing the film data, the number of days, the price and the
     * last availability date known for the film.
     */
    FilmRentalInfoBean filmRentalInfo(FilmModel filmModel, int days);

    /**
     * Computes the availability date for a film checking when was the last time it was rented and if a
     * return is still in progress when it will end.
     * @param filmModel a film
     * @return the expected availability date for the film.
     */
    Date filmAvailabilityDate(FilmModel filmModel);
}
