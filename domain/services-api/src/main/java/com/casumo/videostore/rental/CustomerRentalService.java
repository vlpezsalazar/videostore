package com.casumo.videostore.rental;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.exception.AlreadyRentedException;
import com.casumo.videostore.rental.exception.FilmWasNotRentedByCustomerException;

import java.util.Optional;

public interface CustomerRentalService {
    /**
     * Returns the information regarding the current rental of a film by a customer.
     * This includes computing the pricing surcharges when the return is late.
     *
     * @param customer a customer
     * @param film a film
     * @return An Optional containing the FilmRentedByCustomerModel info with
     * the toPay field populated in case there are surcharges or Optional.empty()
     * in case the customer did not rent that film.
     */
    Optional<FilmRentedByCustomerModel> rentalInfo(CustomerModel customer, FilmModel film);

    /**
     * Creates a new rental of a film by a customer represented by FilmRentedByCustomerModel,
     * setting the rentedDate to today and the expected price of the rental to the corresponding
     * one for the film and days requested.
     *
     * @param customer the customer who rents
     * @param film the film to rent
     * @param days the number of days that the customer will pay for in advance to rent the film.
     * @return a FilmRentedByCustomerModel representing the information.
     * @throws AlreadyRentedException in case a return is in progress, i.e., the film was already
     * rented by a customer (could even be the same customer that called this method).
     */
    FilmRentedByCustomerModel rentFilm(CustomerModel customer, FilmModel film, int days) throws AlreadyRentedException;

    /**
     * Completes an ongoing return for a film. This will retrieve a film rental previously created
     * by rentFilm for the customer setting the returnedOn field to the current date and the toPay
     * field to 0.
     * @param customer the customer who returns the film
     * @param film the film to return
     * @return the updated FilmRentedByCustomerModel stating that the return was completed.
     */
    FilmRentedByCustomerModel returnFilm(CustomerModel customer, FilmModel film) throws FilmWasNotRentedByCustomerException;
}
