package com.casumo.videostore.rental.exception;

public class RentalException extends RuntimeException {

    public RentalException(){}

    public RentalException(String message){
        super(message);
    }
}
