package com.casumo.videostore.rental.exception;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.model.FilmModel;

public class AlreadyRentedException extends RentalException {

    private final Long customerId;
    private final Long filmId;

    public AlreadyRentedException(CustomerModel customerId, FilmModel filmId){
        super("The customer ["+ customerId +"] tried to rent the already rented film  [" + filmId + "]. The Operation was aborted.");
        this.customerId = customerId.id();
        this.filmId = filmId.id();

    }

    public Long getCustomerId() {
        return customerId;
    }

    public Long getFilmId() {
        return filmId;
    }
}
