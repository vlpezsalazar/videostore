package com.casumo.videostore.rental.exception;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.film.model.FilmModel;

public class FilmWasNotRentedByCustomerException extends RentalException {
    public FilmWasNotRentedByCustomerException(CustomerModel customer, FilmModel film) {
        super("The film [" + film + "] was not rented by customer [" + customer + "]");
    }
}
