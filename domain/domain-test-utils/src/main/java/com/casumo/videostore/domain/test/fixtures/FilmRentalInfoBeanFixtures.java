package com.casumo.videostore.domain.test.fixtures;

import com.casumo.videostore.film.bean.FilmRentalInfoBean;

import java.time.Instant;
import java.util.Date;

public final class FilmRentalInfoBeanFixtures {

    private FilmRentalInfoBeanFixtures(){}

    public static FilmRentalInfoBean avengersRentalFor2Days(){
        return FilmRentalInfoBean.builder()
                .film(FilmModelFixtures.avengers())
                .daysToRent(2)
                .price(30)
                .availableOn(Date.from(Instant.now()))
                .build();
    }

    public static FilmRentalInfoBean avengersRentalFor4Days() {
        return FilmRentalInfoBean.builder()
                .film(FilmModelFixtures.avengers())
                .daysToRent(2)
                .price(60)
                .availableOn(Date.from(Instant.now()))
                .build();
    }
}
