package com.casumo.videostore.domain.test.fixtures;

import com.casumo.videostore.customer.model.CustomerModel;

public final class CustomerModelFixtures {

    private CustomerModelFixtures(){}

    public static CustomerModel goodCustomer() {
        return new CustomerModel.Builder()
                .name("good customer")
                .earnedBonusPoints(10)
                .id(100001L)
                .build();
    }

    public static CustomerModel goodCustomerWith11Points() {
        return new CustomerModel.Builder()
                .name("good customer")
                .earnedBonusPoints(11)
                .id(100001L)
                .build();
    }
}
