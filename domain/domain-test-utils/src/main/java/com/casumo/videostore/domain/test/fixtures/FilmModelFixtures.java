package com.casumo.videostore.domain.test.fixtures;

import com.casumo.videostore.film.model.FilmModel;

import static com.casumo.videostore.film.model.FilmType.REGULAR_FILM;

public final class FilmModelFixtures {
    private FilmModelFixtures(){}
    public static FilmModel avengers() {
        return new FilmModel.Builder()
                .filmName("Avengers")
                .filmType(REGULAR_FILM)
                .id(1000L)
                .build();
    }
}
