package com.casumo.videostore.domain.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;

public final class AssertException implements IAssertException {

    private static final IAssertException NO_EXCEPTION_CAPTURED = new IAssertException(){
        @Override
        public IAssertException is(Class<? extends Throwable> exceptionType) {
            assertThat(exceptionType, nullValue());
            return this;
        }

        @Override
        public IAssertException hasNotBeenThrown() {
            return this;
        }
    };

    private final Throwable exception;

    private AssertException(Throwable exception) {
        this.exception = exception;
    }

    @Override
    public IAssertException is(Class<? extends Throwable> exceptionType) {
        assertThat("Exception type thrown was incorrect.", this.exception, instanceOf(exceptionType));
        return this;
    }

    @Override
    public IAssertException hasNotBeenThrown() {
        assertThat("There was an exception thrown: " + exception, false);
        return this;
    }


    public static IAssertException captureException(ExceptionThrow exceptionThrower) {
        try {
            exceptionThrower.throwException();
        } catch (Throwable t){
            t.printStackTrace();
            return new AssertException(t);
        }
        return NO_EXCEPTION_CAPTURED;
    }
}
