package com.casumo.videostore.domain.test;

public interface IAssertException {
    IAssertException is(Class<? extends Throwable> exceptionType);
    IAssertException hasNotBeenThrown();
}
