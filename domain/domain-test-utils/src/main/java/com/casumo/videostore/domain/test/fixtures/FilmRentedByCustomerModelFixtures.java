package com.casumo.videostore.domain.test.fixtures;

import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;

import java.time.Instant;
import java.util.Date;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.time.temporal.ChronoUnit.DAYS;

public final class FilmRentedByCustomerModelFixtures {
    private FilmRentedByCustomerModelFixtures(){}
    public static FilmRentedByCustomerModel goodCustomerRentedAvengersTodayFor2DaysPaying100() {
        return new FilmRentedByCustomerModel.Builder()
                .filmModel(FilmModelFixtures.avengers())
                .customerModel(CustomerModelFixtures.goodCustomer())
                .daysRented(2)
                .paid(100)
                .rentedOn(Date.from(Instant.now()))
                .build();

    }

    public static FilmRentedByCustomerModel goodCustomerRentedAvengers4DaysAgoFor2DaysPayingBasicPrice() {
        return new FilmRentedByCustomerModel.Builder()
                .filmModel(FilmModelFixtures.avengers())
                .customerModel(CustomerModelFixtures.goodCustomer())
                .daysRented(2)
                .paid(30)
                .rentedOn(Date.from(Instant.now().minus(4, DAYS)))
                .build();
    }
    public static FilmRentedByCustomerModel goodCustomerRentedAvengersFor2DaysPayingBasicPriceOn(Date rentedOn) {
        return new FilmRentedByCustomerModel.Builder()
                .filmModel(FilmModelFixtures.avengers())
                .customerModel(CustomerModelFixtures.goodCustomer())
                .daysRented(2)
                .paid(30)
                .rentedOn(rentedOn)
                .build();
    }

    public static Map<Boolean, List<Object>> generateASetOfGoodCustomerRentedAvengersFor2DaysPayingBasicPriceAndABadFilmOn(Date rentedOn) {
        final HashMap<Boolean, List<Object>> rentals = new HashMap<>();
        rentals.put(true, Collections.singletonList(goodCustomerRentedAvengersFor2DaysPayingBasicPriceOn(rentedOn)));
        rentals.put(false, Collections.singletonList("BadFilm"));
        return rentals;

    }
}
