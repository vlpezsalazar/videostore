package com.casumo.videostore.domain.test;

@FunctionalInterface
public interface ExceptionThrow {
    void throwException() throws Throwable;
}
