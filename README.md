VideoStore REST Services.
--

##Project Description

This project aims to represent the services of a video store. 


##Project Structure

It is structured in the following way:


* /acceptance-test: a cucumber specification for the functionality of the video store. Currently it just holds some 
scenarios and the glue code is pending. It was aimed to run against a mysql database which is raised when the task 
_./gradlew startDatabase_ is invoked. The task defined as acceptanceTest or aT.

* /config: this folder is intended to contain configuration files (properties) for different environments. Currently 
only dev is there. In case you want to add a new environment, just create a new folder here with your environment name, 
then, to use these configurations, invoke any module with ../gradle -P env=<environment_folder>. The default environment
is configured in the file gradle.properties.

* /infrastructure: this folder contains scripts to provision several docker containers. Those are defined in their own 
folders and can be invoked using the proper script contained in the folder:
    * /database: contains a script that provisions a dockerized mysql image and creates the container 
configured with the values passed as parameters. ATs uses this script to start up the DB with the values defined in the
configuration. 
    * /swagger-ui: contains a script to create a swagger container useful to check the rest documentation.


* /domain: this folder contains the business domain model and logic. It's contained in it's own module to maximize the
 isolation of concerns between what is business logic and what is specifically technical like the frameworks used for 
 persistence or REST. In fact the number of dependencies allowed in this module should be kept as a minimum. 
 It defines a set of interfaces which specifies the following:
    * /model: this holds the definition of the business entities Customer, Film and Rental, their relationships and the
     DAOs allowing CRUD operations on them. Mostly done using immutables (immutables.io).
    * /services-api: interfaces holding several operations in the videostore domain that a customer can do on films. 
    * /services-impl: an implementation of these operations.
    * /domain-test-utils: several utilities handy for creating model fixtures and testing.
    
* persistence: configuration for a persistence layer. It holds the concrete implementation of the models and DAOs in the
 domain. Actually, I have used a JPA annotations & Spring Data to create this layer. It also contains the configuration 
 for a DB Migration tool: flyway which is able to manage database schema versions defined in a set of files on the 
 src/resources/script folder. Each schema change is defined using a file in this folder.
 
* rest: the rest layer done using Spring boot + Jersey + Jackson + Orika. It is structured in the following way:
    * rest-api: holds the specs for the rest endpoints (paths, operations...) by means of annotated interfaces using
    jax-rs annotations and swagger to document those.
    * rest-services: the actual implementation of the services.
    * rest-test-utils: some fixtures useful for testing the services.
    
##Compiling & Building
$ ./gradlew clean build -> clean previous artifacts and build the solution.

##Running

$ ./gradlew startDatabase
$ ./gradlew bootRun

  
##Running tests
The project holds unit tests for most of the hand-coded functionality in each of the services and a integration test
 for the happy paths in the rental services. In order to run them you can do:
 
$ ./gradlew rest:rest-services:integrationTests

#Accessing documentation
Besides the interfaces being documented using javadoc, you can access to a swagger documentation of the rest api. 
To do that, you need the rest-services active and to start the swagger container up doing:

$ ./gradlew swaggerui
