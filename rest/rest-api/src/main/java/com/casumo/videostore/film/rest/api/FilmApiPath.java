package com.casumo.videostore.film.rest.api;


import javax.ws.rs.core.UriBuilder;

public final class FilmApiPath {

    private FilmApiPath() {
    }

    public static final String DAYS_PARAM = "days";

    public static final String FILM_PATH_PARAM = "film_token";

    public static final String FILM = "/{" + FILM_PATH_PARAM + "}";

    public static final String FILMS = "/films";

    public static String FILM(String basePath, String filmToken) {
        return UriBuilder.fromPath(basePath).path(FILMS).path(FILM)
                .resolveTemplate(FILM_PATH_PARAM, filmToken)
                .toTemplate();
    }

    public static String FILM(String basePath, String filmToken, int days) {
        return UriBuilder.fromPath(basePath).path(FILMS).path(FILM).queryParam(DAYS_PARAM, days)
                .resolveTemplate(FILM_PATH_PARAM, filmToken)
                .toTemplate();
    }
}
