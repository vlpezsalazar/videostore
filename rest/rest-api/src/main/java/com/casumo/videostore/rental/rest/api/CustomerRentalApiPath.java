package com.casumo.videostore.rental.rest.api;


import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;

import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER;
import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMERS;
import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER_PATH_PARAM;

public final class CustomerRentalApiPath {

    private CustomerRentalApiPath() {}

    public static final String RENTAL_PATH = "/rentals";

    public static final String FILM_QUERY_PARAM = "film";

    public static final String CUSTOMER_RENTAL_PATH = CUSTOMERS + CUSTOMER + RENTAL_PATH;

    public static Invocation.Builder CUSTOMER_RENTAL(WebTarget target, String customerToken){
        return target.path(CUSTOMER_RENTAL_PATH)
                .resolveTemplate(CUSTOMER_PATH_PARAM, customerToken)
                .request();

    }
}
