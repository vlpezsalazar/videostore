package com.casumo.videostore.rental.rest.api;

import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalRequest;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.jaxrs.PATCH;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER_PATH_PARAM;
import static com.casumo.videostore.rental.rest.api.CustomerRentalApiPath.FILM_QUERY_PARAM;
import static com.casumo.videostore.rental.rest.api.media.CustomerRentalMediaType.CASUMO_VIDEO_STORE_FILM_RENTAL_JSON;

@Produces(CASUMO_VIDEO_STORE_FILM_RENTAL_JSON)
public interface CustomerRentalRestApi {

    @GET
    @Consumes(CASUMO_VIDEO_STORE_FILM_RENTAL_JSON)
    @ApiOperation("Provides information about a set of previous customer's films rentals, computing " +
            "the surcharges for late return for those films that are past the prepaid period.")
    @ApiResponses({
            @ApiResponse(code = 200,
                    message = "The information relative to the set of films rented.",
                    response = CustomerRentalInfoResponse.class),
            @ApiResponse(code = 404, message = "In case the customerToken didn't identified any customer in the system")

    })
    CustomerRentalInfoResponse customerRentalInfo(@ApiParam(value = "Customer token", required = true) @PathParam(CUSTOMER_PATH_PARAM) String customer,
                                                  @ApiParam("Films tokens") @QueryParam(FILM_QUERY_PARAM) List<String> films);

    @POST
    @Consumes(CASUMO_VIDEO_STORE_FILM_RENTAL_JSON)
    @ApiOperation("Rents a set of films.")
    @ApiResponses({
            @ApiResponse(code = 202,
                    message = "The information relative to the set of films rented including " +
                            "a grand total with the price prepaid by the customer.",
                    response = CustomerRentalInfoResponse.class),
            @ApiResponse(code = 404, message = "In case the customerToken didn't identified any customer in the system")

    })
    Response rentFilms(@ApiParam(value = "Customer token", required = true) @PathParam(CUSTOMER_PATH_PARAM) String customer,
                       @ApiParam(value = "List of films to rent", required = true) @NotNull List<FilmRentalRequest> films);

    @PATCH
    @Consumes(CASUMO_VIDEO_STORE_FILM_RENTAL_JSON)
    @ApiOperation("Returns a set of films.")
    @ApiResponses({
            @ApiResponse(
                    code = 202,
                    message = "The information relative to the set of films returned including " +
                            "a grand total with the price prepaid by the customer.",
                    response = CustomerRentalInfoResponse.class),
            @ApiResponse(code = 404, message = "In case the customerToken didn't identified any customer in the system")

    })
    Response returnFilms(@ApiParam(value = "Customer token", required = true) @PathParam(CUSTOMER_PATH_PARAM) String customer,
                         @ApiParam(value = "List of films to return", required = true) @NotNull List<String> films);
}
