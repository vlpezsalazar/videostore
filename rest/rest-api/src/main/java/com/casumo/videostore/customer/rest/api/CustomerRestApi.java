package com.casumo.videostore.customer.rest.api;

import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;
import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER;
import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER_PATH_PARAM;
import static com.casumo.videostore.customer.rest.api.media.CustomerMediaType.CASUMO_VIDEO_STORE_CUSTOMER_JSON;

@Produces(CASUMO_VIDEO_STORE_CUSTOMER_JSON)
public interface CustomerRestApi {

    @POST
    @Consumes(CASUMO_VIDEO_STORE_CUSTOMER_JSON)
    @ApiOperation("This call creates a new customer.")
    @ApiResponse(code = 201,
            message = "The token that identifies the created customer within requests in the LOCATION header.")
    Response createCustomer(@ApiParam(value = "Customer token", required = true) CustomerCreationRequest customerDefinition);

    @GET
    @Path(CUSTOMER)
    @ApiOperation("This call finds and retrieves the information of a customer.")
    @Produces(CASUMO_VIDEO_STORE_CUSTOMER_JSON)
    @ApiResponses({
            @ApiResponse(code = 200,
                    message = "The token that identifies a customer.",
                    response = CustomerDescriptionResponse.class),
            @ApiResponse(code = 404,
                    message = "When the customerToken didn't identify any customer in the system.")
    })
    CustomerDescriptionResponse lookupCustomer(@ApiParam(value = "Customer token, same one as returned by a POST to this resource", required = true) @PathParam(CUSTOMER_PATH_PARAM) String customerToken);

    @DELETE
    @Path(CUSTOMER)
    @ApiOperation("This call deletes the customer identified by customerToken from the system.")
    @ApiResponses({
            @ApiResponse(code = 204, message = "An empty response"),
            @ApiResponse(code = 404,
                    message = "When the customerToken didn't identify any customer in the system.")
    })
    Response deleteCustomer(@ApiParam(value = "Customer token", required = true) @PathParam(CUSTOMER_PATH_PARAM) String customerToken);
}
