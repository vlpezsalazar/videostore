package com.casumo.videostore.customer.rest.api.media;

public final class CustomerMediaType {
    private CustomerMediaType(){}

    public static final String CASUMO_VIDEO_STORE_CUSTOMER_JSON = "application/video_store_customer.v0+json";
}
