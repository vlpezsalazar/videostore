package com.casumo.videostore.rental.rest.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.Collection;

@Value.Style(visibility = Value.Style.ImplementationVisibility.PACKAGE, overshadowImplementation = true)
@Value.Immutable
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(builder = CustomerRentalInfoResponse.Builder.class)
@JsonSerialize
public interface CustomerRentalInfoResponse {

    Collection<CustomerRentalInfoDto> filmsRented();

    Collection<FilmRentalNotFoundDto> errors();

    double totalPrice();

    static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder(withPrefix = "")
    class Builder extends ImmutableCustomerRentalInfoResponse.Builder {}
}
