package com.casumo.videostore.rental.rest.api.dto;

import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;
import java.util.Date;

@Value.Style(allParameters = true, of = "new", visibility = Value.Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Value.Immutable
@JsonDeserialize(builder = CustomerRentalInfoDto.Builder.class)
public interface CustomerRentalInfoDto {
    FilmDescriptionDto film();
    int daysRented();
    double toPay();
    double paid();

    Date rentedOn();

    @Nullable
    Date returnedOn();

    static Builder builder() {
        return new Builder();
    }

    class Builder extends ImmutableCustomerRentalInfoDto.Builder {}


}
