package com.casumo.videostore.rental.rest.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Style(visibility = Value.Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Value.Immutable
@JsonDeserialize(builder = FilmRentalRequest.Builder.class)
public interface FilmRentalRequest {
    String film();

    int daysToRent();

    static FilmRentalRequest of(String film, int daysToRent){
        return new Builder().film(film).daysToRent(daysToRent).build();
    }

    class Builder extends ImmutableFilmRentalRequest.Builder {}
}
