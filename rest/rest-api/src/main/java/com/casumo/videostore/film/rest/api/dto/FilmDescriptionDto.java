package com.casumo.videostore.film.rest.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.immutables.value.Value;

import static org.immutables.value.Value.Style.ImplementationVisibility.PUBLIC;

@Value.Style(allParameters = true, of = "new", visibility = PUBLIC, overshadowImplementation = true)
@Value.Immutable
@JsonDeserialize(builder = FilmDescriptionDto.Builder.class)
@ApiModel
public interface FilmDescriptionDto {

    @ApiModelProperty(name = "name")
    String name();

    static Builder builder() {
        return new Builder();
    }

    class Builder extends ImmutableFilmDescriptionDto.Builder {}
}
