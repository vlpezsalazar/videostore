package com.casumo.videostore.rental.rest.api.media;

public final class CustomerRentalMediaType {
    private CustomerRentalMediaType(){}

    public static final String CASUMO_VIDEO_STORE_FILM_RENTAL_JSON = "application/video_store_customer_rental.v0+json";
}
