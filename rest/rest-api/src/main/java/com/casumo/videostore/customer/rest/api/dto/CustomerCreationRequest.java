package com.casumo.videostore.customer.rest.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import static org.immutables.value.Value.Style.ImplementationVisibility.PACKAGE;

@Value.Style(visibility = PACKAGE, overshadowImplementation = true)
@Value.Immutable
@JsonDeserialize(builder = CustomerCreationRequest.Builder.class)
public interface CustomerCreationRequest {
    String name();

    static Builder builder(){
        return new Builder();
    }

    class Builder extends ImmutableCustomerCreationRequest.Builder {}
}
