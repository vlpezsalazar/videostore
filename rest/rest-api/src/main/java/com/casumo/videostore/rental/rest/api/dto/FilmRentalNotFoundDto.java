package com.casumo.videostore.rental.rest.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ImmutableFilmRentalNotFoundDto.Builder.class)
public interface FilmRentalNotFoundDto {
    String reason();

    static FilmRentalNotFoundDto of(String reason){
        return ImmutableFilmRentalNotFoundDto.builder()
                .reason(reason)
                .build();
    }

}
