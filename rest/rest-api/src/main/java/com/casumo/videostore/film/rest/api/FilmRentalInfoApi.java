package com.casumo.videostore.film.rest.api;


import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalInfoDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import static com.casumo.videostore.film.rest.api.FilmApiPath.DAYS_PARAM;
import static com.casumo.videostore.film.rest.api.FilmApiPath.FILM;
import static com.casumo.videostore.film.rest.api.FilmApiPath.FILM_PATH_PARAM;
import static com.casumo.videostore.film.rest.api.media.FilmsMediaType.CASUMO_VIDEO_STORE_FILM_JSON;

public interface FilmRentalInfoApi {
    @GET
    @Path(FILM)
    @Produces(CASUMO_VIDEO_STORE_FILM_JSON)
    @ApiOperation("Provides information about a film, the availability date and the price to rent it for the days required")
    @ApiResponses({
            @ApiResponse(code = 200, message = "The information of the films rented.", response = CustomerRentalInfoResponse.class),
            @ApiResponse(code = 404, message = "In case the filmToken didn't identify any film.")
    })
    FilmRentalInfoDto filmRentalInfo(@ApiParam(value = "Film token to get the rental information", name = "film") @PathParam(FILM_PATH_PARAM) String filmToken,
                                     @ApiParam(value = "The number of days to rent the film", name = "daysToRent")  @QueryParam(DAYS_PARAM) int daysToRent) throws NotFoundException;
}


