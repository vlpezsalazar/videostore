package com.casumo.videostore.film.rest.api;

import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static com.casumo.videostore.film.rest.api.FilmApiPath.FILM;
import static com.casumo.videostore.film.rest.api.FilmApiPath.FILM_PATH_PARAM;
import static com.casumo.videostore.film.rest.api.media.FilmsMediaType.CASUMO_VIDEO_STORE_FILM_JSON;

@Produces(CASUMO_VIDEO_STORE_FILM_JSON)
public interface FilmRestApi {

    @POST
    @Consumes(CASUMO_VIDEO_STORE_FILM_JSON)
    @ApiOperation("This call creates a new film in the videostore.")
    @ApiResponses({
            @ApiResponse(code = 201,
                    message = "The token that identifies the created film within requests in the LOCATION header."),
            @ApiResponse(code =500,
                    message = "In case some error happened.")
    })
    Response addFilm(@ApiParam(value = "Description of the film to add to the system") FilmDescriptionDto filmDto);

    @DELETE
    @Path(FILM)
    @ApiOperation("This call deletes the film identified by filmToken from the system.")
    @ApiResponses({
            @ApiResponse(code = 204, message = "An empty response"),
            @ApiResponse(code = 404, message = "when the filmToken didn't identify any film.,")

    })
    Response deleteFilm(@ApiParam(value = "Film (token) to remove") @PathParam(FILM_PATH_PARAM) String filmToken);

}
