package com.casumo.videostore.customer.rest.api;


import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static com.casumo.videostore.customer.rest.api.media.CustomerMediaType.CASUMO_VIDEO_STORE_CUSTOMER_JSON;

public final class CustomerApiPath {

    private CustomerApiPath() {
    }

    public static final String CUSTOMER_PATH_PARAM = "customer_token";

    public static final String CUSTOMER = "/{" + CUSTOMER_PATH_PARAM + "}";

    public static final String CUSTOMERS = "/customers";

    public static Invocation.Builder CUSTOMER(WebTarget webTarget, String token) {
        return webTarget.path(CUSTOMERS).path(CUSTOMER)
                .resolveTemplate(CUSTOMER_PATH_PARAM, token)
                .request();
    }
    public static Response createCustomer(WebTarget webTarget, CustomerCreationRequest request) {
        return webTarget.path(CUSTOMERS)
                .request().post(Entity.entity(request, CASUMO_VIDEO_STORE_CUSTOMER_JSON));
    }

}
