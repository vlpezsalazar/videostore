package com.casumo.videostore.film.rest.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.Arrays;
import java.util.Collection;

@Value.Immutable
@JsonDeserialize(builder = ImmutableFilmResponse.Builder.class)
public interface FilmResponse {
    Collection<FilmDescriptionDto> films();

    static FilmResponse of(FilmDescriptionDto... films){
        return ImmutableFilmResponse.builder().films(Arrays.asList(films)).build();
    }
}
