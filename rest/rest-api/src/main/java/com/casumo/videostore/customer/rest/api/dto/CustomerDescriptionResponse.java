package com.casumo.videostore.customer.rest.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.immutables.value.Value;

import static org.immutables.value.Value.Style.ImplementationVisibility.PUBLIC;

@Value.Style(allParameters = true, of = "new", visibility = PUBLIC, overshadowImplementation = true)
@Value.Immutable
@JsonSerialize
@JsonDeserialize(builder = CustomerDescriptionResponse.Builder.class)
@ApiModel
public interface CustomerDescriptionResponse {

    @ApiModelProperty
    String name();

    @ApiModelProperty
    long earnedBonusPoints();

    class Builder extends ImmutableCustomerDescriptionResponse.Builder {}

    static CustomerDescriptionResponse of(String name){
        return new Builder().name(name).build();
    }
}
