package com.casumo.videostore.rental.rest.api.dto;

import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.immutables.value.Value;

import java.time.Instant;
import java.util.Date;

@Value.Style(allParameters = true, of = "new", visibility = Value.Style.ImplementationVisibility.PUBLIC, overshadowImplementation = true)
@Value.Immutable
@JsonSerialize
@ApiModel(description = "Information regarding the rental of a film.")
public interface FilmRentalInfoDto {
    @ApiModelProperty(name = "film")
    FilmDescriptionDto film();

    @ApiModelProperty(name="daysToRent")
    int daysToRent();

    @ApiModelProperty(name="price")
    double price();

    @ApiModelProperty
    @JsonSerialize
    @Value.Default
    default Date availableOn(){
        return Date.from(Instant.now());
    }

    static Builder builder() {
        return new Builder();
    }


    class Builder extends ImmutableFilmRentalInfoDto.Builder{}

}
