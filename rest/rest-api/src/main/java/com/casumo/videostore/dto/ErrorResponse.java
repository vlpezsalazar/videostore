package com.casumo.videostore.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonDeserialize(builder = ErrorResponse.Builder.class)
public interface ErrorResponse {
    String message();

    static Builder builder(){
        return new Builder();
    }

    class Builder extends ImmutableErrorResponse.Builder {}
}
