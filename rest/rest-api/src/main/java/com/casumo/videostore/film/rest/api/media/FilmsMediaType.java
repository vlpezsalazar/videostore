package com.casumo.videostore.film.rest.api.media;

public final class FilmsMediaType {

    private FilmsMediaType(){}

    public static final String CASUMO_VIDEO_STORE_FILM_JSON = "application/video_store_films.v0+json";
}
