package com.casumo.videostore.film.rest.api;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FilmApiPathTest {
    @Test
    public void FILM_shouldCreateAPathForFilmResource() throws Exception {
        //given
        final String customer_token = "blah";

        //when
        final String customerPath = FilmApiPath.FILM("blah/", customer_token);

        //then
        assertThat(customerPath, is("blah/films/" + customer_token));
    }

}