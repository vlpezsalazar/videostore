package com.casumo.videostore.rest.test.fixtures;

import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoDto;

import java.time.Instant;
import java.util.Date;

public final class CustomerRentalInfoDtoFixtures {

    public static CustomerRentalInfoDto goodCustomerRentedAvengersTodayFor2DaysPaying100() {
        return new CustomerRentalInfoDto.Builder()
                .film(FilmDescriptionDtoFixtures.avengers())
                .paid(100d)
                .toPay(0)
                .rentedOn(Date.from(Instant.now()))
                .daysRented(2)
                .build();
    }

    public static CustomerRentalInfoDto goodCustomerRentedAvengersFor2DaysPaying100On(Date rentedOn) {
        return new CustomerRentalInfoDto.Builder()
                .film(FilmDescriptionDtoFixtures.avengers())
                .paid(100d)
                .toPay(0)
                .rentedOn(rentedOn)
                .daysRented(2)
                .build();
    }

    public static CustomerRentalInfoDto goodCustomerRentedAvengersTodayFor2DaysPayingBasicPriceOn(Date rentedOn) {
        return new CustomerRentalInfoDto.Builder()
                .film(FilmDescriptionDtoFixtures.avengers())
                .paid(30d)
                .toPay(0)
                .rentedOn(rentedOn)
                .daysRented(2)
                .build();
    }
}
