package com.casumo.videostore.rest.test.fixtures;

import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalRequest;

public class FilmRentalRequestFixtures {
    private static final String avengersToken = String.valueOf(FilmModelFixtures.avengers().id());

    public static FilmRentalRequest rentalRequestAvengersFor2Days() {
        return new FilmRentalRequest.Builder()
                .film(avengersToken)
                .daysToRent(2)
                .build();
    }
}
