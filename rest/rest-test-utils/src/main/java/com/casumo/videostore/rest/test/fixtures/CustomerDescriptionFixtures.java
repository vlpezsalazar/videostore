package com.casumo.videostore.rest.test.fixtures;

import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;

public class CustomerDescriptionFixtures {
    public static CustomerDescriptionResponse goodCustomer() {
        return CustomerDescriptionResponse.of("good customer");
    }
}
