package com.casumo.videostore.rest.test.fixtures;

import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;

public final class FilmDescriptionDtoFixtures {

    private FilmDescriptionDtoFixtures(){};

    public static FilmDescriptionDto avengers() {
        return new FilmDescriptionDto.Builder()
                .name("Avengers")
                .build();
    }
}
