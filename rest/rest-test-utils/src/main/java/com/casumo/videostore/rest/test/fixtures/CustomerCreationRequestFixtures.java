package com.casumo.videostore.rest.test.fixtures;

import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;

public final class CustomerCreationRequestFixtures {

    private CustomerCreationRequestFixtures(){}

    public static CustomerCreationRequest goodCustomer() {
        return CustomerCreationRequest.builder().name("good customer").build();
    }
}
