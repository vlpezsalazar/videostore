package com.casumo.videostore.customer.rest.dto.mapper;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;
import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;
import ma.glasnost.orika.BoundMapperFacade;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {

    private final BoundMapperFacade<CustomerCreationRequest, CustomerModel> mapper;

    private final BoundMapperFacade<CustomerModel, CustomerDescriptionResponse> mapper1;

    public CustomerMapper(BoundMapperFacade<CustomerCreationRequest, CustomerModel> mapper, BoundMapperFacade<CustomerModel, CustomerDescriptionResponse> mapper1) {
        this.mapper = mapper;
        this.mapper1 = mapper1;
    }

    public CustomerModel map(CustomerCreationRequest customerData) {
        return mapper.map(customerData);

    }

    public CustomerDescriptionResponse map(CustomerModel customerModel) {
        return mapper1.map(customerModel);
    }

}
