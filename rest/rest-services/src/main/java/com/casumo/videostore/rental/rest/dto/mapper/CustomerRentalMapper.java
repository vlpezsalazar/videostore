package com.casumo.videostore.rental.rest.dto.mapper;

import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoDto;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalNotFoundDto;
import ma.glasnost.orika.BoundMapperFacade;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CustomerRentalMapper {

    private final BoundMapperFacade<FilmRentedByCustomerModel, CustomerRentalInfoDto> mapper;

    public CustomerRentalMapper(BoundMapperFacade<FilmRentedByCustomerModel, CustomerRentalInfoDto> mapper){
        this.mapper = mapper;
    }

    public CustomerRentalInfoDto map(FilmRentedByCustomerModel filmRentedByCustomerModel) {
        return mapper.map(filmRentedByCustomerModel);
    }


    public CustomerRentalInfoResponse map(Map<Boolean, List<Object>> filmsRentedByCustomer, double totalPrice){
        return CustomerRentalInfoResponse.builder()
                .totalPrice(totalPrice)
                .filmsRented(filmsRentedByCustomer.get(true).stream().map(rental -> map((FilmRentedByCustomerModel) rental)).collect(Collectors.toList()))
                .errors(filmsRentedByCustomer.get(false).stream().map(reason -> FilmRentalNotFoundDto.of((String) reason)).collect(Collectors.toList()))
                .build();
    }
}
