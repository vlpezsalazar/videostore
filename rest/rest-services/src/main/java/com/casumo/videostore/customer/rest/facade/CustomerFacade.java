package com.casumo.videostore.customer.rest.facade;

import com.casumo.videostore.customer.dao.CustomerDao;
import com.casumo.videostore.customer.model.CustomerModel;
import org.springframework.stereotype.Component;

import javax.ws.rs.NotFoundException;
import java.util.Optional;

@Component
public class CustomerFacade {

    private final CustomerDao customerDao;

    public CustomerFacade(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public String createCustomer(final CustomerModel customerInfo) {
        final CustomerModel persistedEntity = customerDao.persist(customerInfo);

        return String.valueOf(persistedEntity.id());
    }

    public CustomerModel find(final String customerToken) {
        final Optional<CustomerModel> customerModel = customerDao.findById(Long.parseLong(customerToken));
        return customerModel
                .orElseThrow(() -> new NotFoundException(String.format("The customer represented by [%s] was not found.", customerToken)));
    }

    public void deleteCustomer(final String customerToken) {
        final Long deleteById = customerDao.deleteById(Long.parseLong(customerToken));
        if (deleteById <= 0) throw new NotFoundException(String.format("The customer represented by [%s] was not found.", customerToken));

    }
}
