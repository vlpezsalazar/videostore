package com.casumo.videostore.film.rest.resource;

import com.casumo.videostore.film.rest.api.FilmRentalInfoApi;
import com.casumo.videostore.film.rest.dto.mapper.FilmMapper;
import com.casumo.videostore.film.rest.facade.FilmFacade;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalInfoDto;
import com.casumo.videostore.rest.hateoas.HATEOAS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;

import static com.casumo.videostore.film.rest.api.FilmApiPath.FILMS;

@Service
@HATEOAS
@Api(tags = "films")
@Path(FILMS)
public class FilmRentalInfoResource implements FilmRentalInfoApi {

    private final FilmFacade filmFacade;

    private final FilmMapper mapper;

    public FilmRentalInfoResource(FilmFacade filmFacade, FilmMapper mapper) {
        this.filmFacade = filmFacade;
        this.mapper = mapper;
    }

    @Override
    public FilmRentalInfoDto filmRentalInfo(@ApiParam(value = "Film token to get the rental information", name = "film") String film,
                                            @ApiParam(value = "The number of days to rent the film", name = "daysToRent") int daysToRent) throws NotFoundException {

        return mapper.map(filmFacade.filmRentalInfo(film, daysToRent));
    }
}
