package com.casumo.videostore.customer.rest.resource;

import com.casumo.videostore.customer.rest.api.CustomerRestApi;
import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;
import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;
import com.casumo.videostore.customer.rest.dto.mapper.CustomerMapper;
import com.casumo.videostore.customer.rest.facade.CustomerFacade;
import com.casumo.videostore.rest.hateoas.HATEOAS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER;
import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMERS;
import static com.casumo.videostore.rest.configuration.JerseyConfig.VIDEOSTORE_REST;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;

@Service
@HATEOAS
@Api("customers")
@Path(CUSTOMERS)
public class CustomerManagementResource implements CustomerRestApi {

    private final CustomerFacade customerFacade;

    private final CustomerMapper mapper;

    public CustomerManagementResource(CustomerFacade customerFacade, CustomerMapper mapper){
        this.customerFacade = customerFacade;
        this.mapper = mapper;
    }

    @Override
    public Response createCustomer(@ApiParam(value = "Customer token", required = true)  CustomerCreationRequest customerData){
        final String customerId = customerFacade.createCustomer(mapper.map(customerData));
        return Response.created(UriBuilder.fromPath(VIDEOSTORE_REST).path(CustomerManagementResource.class).path(CUSTOMER).build(customerId)).build();
    }

    @Override
    public CustomerDescriptionResponse lookupCustomer(@ApiParam(value = "Customer token, same one as returned by a POST to this resource", required = true) String customerToken){
        return mapper.map(customerFacade.find(customerToken));
    }

    @Override
    public Response deleteCustomer(@ApiParam(value = "Customer token", required = true) String customer) throws NotFoundException {
        customerFacade.deleteCustomer(customer);
        return Response.status(NO_CONTENT).build();
    }


}
