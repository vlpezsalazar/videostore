package com.casumo.videostore.rest.configuration;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.property.RegexPropertyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.inject.Singleton;

@Configuration
public class MapperFactoryConfiguration {

    @Singleton
    @Bean
    public MapperFactory mapperFactory() {
        return new DefaultMapperFactory.Builder()
                .propertyResolverStrategy(
                        new RegexPropertyResolver(
                                "([\\w]+)",
                                "([\\w]+)",
                                true, true))
                .build();
    }
}
