package com.casumo.videostore.film.rest.dto.mapper;

import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalInfoDto;
import ma.glasnost.orika.BoundMapperFacade;
import org.springframework.stereotype.Component;

@Component
public class FilmMapper {
    private final BoundMapperFacade<FilmModel, FilmDescriptionDto> mapper;
    private final BoundMapperFacade<FilmRentalInfoBean, FilmRentalInfoDto> mapper2;

    public FilmMapper(BoundMapperFacade<FilmModel, FilmDescriptionDto> mapper, BoundMapperFacade<FilmRentalInfoBean, FilmRentalInfoDto> mapper2) {
        this.mapper = mapper;
        this.mapper2 = mapper2;
    }

    public FilmModel map(FilmDescriptionDto filmDto) {
        return mapper.mapReverse(filmDto);
    }

    public FilmDescriptionDto map(FilmModel filmDto) {
        return mapper.map(filmDto);
    }

    public FilmRentalInfoDto map(FilmRentalInfoBean rentFilmDto) {
        return mapper2.map(rentFilmDto);
    }
}
