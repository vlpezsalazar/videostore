package com.casumo.videostore.rest.hateoas;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RestResponseWrapper<Type> extends ResourceSupport {
    private Type content;

    @JsonCreator
    public RestResponseWrapper(@JsonProperty("content") Type content) {
        this.content = content;
    }

    public Type getContent(){
        return content;
    }

}
