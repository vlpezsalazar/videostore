package com.casumo.videostore.film.rest.facade;

import com.casumo.videostore.film.FilmInfoService;
import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.dao.FilmDao;
import com.casumo.videostore.film.model.FilmModel;
import org.springframework.stereotype.Component;

import javax.ws.rs.NotFoundException;
import java.util.Optional;

@Component
public class FilmFacade {

    private final FilmDao filmDao;

    private final FilmInfoService filmInfoService;

    public FilmFacade(FilmDao filmDao, FilmInfoService filmInfoService) {
        this.filmDao = filmDao;
        this.filmInfoService = filmInfoService;
    }

    public String createFilm(FilmModel filmToken) {
        final FilmModel persistedEntity = filmDao.persist(filmToken);

        return String.valueOf(persistedEntity.id());
    }

    public FilmModel findFilm(String filmToken)  {
        final Optional<FilmModel> filmModel = filmDao.findById(Long.parseLong(filmToken));
        return filmModel
                .orElseThrow(() -> new NotFoundException(String.format("The film represented by [%s] was not found", filmToken)));
    }

    public void deleteFilm(String filmToken)  {
        final Long deleted = filmDao.deleteById(Long.parseLong(filmToken));
        if (deleted < 0) throw new NotFoundException(String.format("The film represented by [%s] was not found", filmToken));
    }

    public FilmRentalInfoBean filmRentalInfo(String filmToken, int daysToRent){
        final FilmModel film = findFilm(filmToken);
        return filmInfoService.filmRentalInfo(film, daysToRent);
    }
}
