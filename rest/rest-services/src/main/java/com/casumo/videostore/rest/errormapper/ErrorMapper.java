package com.casumo.videostore.rest.errormapper;

import com.casumo.videostore.dto.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Component
public class ErrorMapper implements ExceptionMapper<Exception> {

    private final static Logger logger = LoggerFactory.getLogger(ErrorMapper.class);

    @Override
    public Response toResponse(Exception exception) {
        logger.error("Uncaptured exception", exception);
        return Response.serverError()
                .type(MediaType.APPLICATION_JSON)
                .entity(ErrorResponse
                        .builder()
                        .message(exception.getMessage())
                        .build())
                .build();
    }
}
