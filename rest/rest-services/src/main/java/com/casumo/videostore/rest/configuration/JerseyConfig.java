package com.casumo.videostore.rest.configuration;

import com.casumo.videostore.rest.cors.CORSFilter;
import com.casumo.videostore.rest.hateoas.RestResponseFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import javax.ws.rs.ApplicationPath;

@Configuration
@ApplicationPath(JerseyConfig.VIDEOSTORE_REST)
@Lazy
public class JerseyConfig extends ResourceConfig {

    private static final String VERSION = "v0";
    public static final String VIDEOSTORE_REST = "/videostore/rest/" + VERSION;

    public JerseyConfig() {
        register(RequestContextFilter.class);
        register(RestResponseFilter.class);
        packages("com.casumo.videostore");
        register(LoggingFeature.class);
        register(JacksonFeature.class);
        register(new CORSFilter());
        configureSwagger();
    }

    void configureSwagger(){
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("Video Store");
        beanConfig.setVersion(VERSION);
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setBasePath("/api-docs");
        beanConfig.setResourcePackage("com.casumo.videostore");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper;
    }

}