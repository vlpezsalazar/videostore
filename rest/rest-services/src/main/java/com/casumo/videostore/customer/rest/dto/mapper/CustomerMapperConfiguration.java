package com.casumo.videostore.customer.rest.dto.mapper;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.model.ImmutableCustomerModel;
import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;
import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;
import com.casumo.videostore.customer.rest.api.dto.ImmutableCustomerDescriptionResponse;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerMapperConfiguration extends ConfigurableMapper {

    private final MapperFactory mapperFactory;

    @Autowired
    public CustomerMapperConfiguration(MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;
        mapperFactory.classMap(CustomerCreationRequest.class, ImmutableCustomerModel.class)
                .byDefault()
                .register();

        mapperFactory.registerConcreteType(CustomerModel.class, ImmutableCustomerModel.class);

        mapperFactory.classMap(CustomerModel.class, ImmutableCustomerDescriptionResponse.class)
                .byDefault()
                .register();

        mapperFactory.registerConcreteType(CustomerDescriptionResponse.class, ImmutableCustomerDescriptionResponse.class);
    }

    @Bean
    public BoundMapperFacade<CustomerCreationRequest, CustomerModel> mapperCustomerCreationRequestToCustomerModel(){
        return mapperFactory.getMapperFacade(CustomerCreationRequest.class, CustomerModel.class);
    }

    @Bean
    public BoundMapperFacade<CustomerModel, CustomerDescriptionResponse> mapperCustomerCustomerModelToCustomerDescriptionResponse(){
        return mapperFactory.getMapperFacade(CustomerModel.class, CustomerDescriptionResponse.class);
    }
}
