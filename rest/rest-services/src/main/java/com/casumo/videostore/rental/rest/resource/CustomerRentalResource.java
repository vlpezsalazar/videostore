package com.casumo.videostore.rental.rest.resource;

import com.casumo.videostore.rental.CustomerRentalService;
import com.casumo.videostore.rental.exception.RentalException;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.rest.api.CustomerRentalRestApi;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalRequest;
import com.casumo.videostore.rental.rest.dto.mapper.CustomerRentalMapper;
import com.casumo.videostore.rental.rest.facade.CustomerRentalFacade;
import com.casumo.videostore.rest.hateoas.HATEOAS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static com.casumo.videostore.rental.rest.api.CustomerRentalApiPath.CUSTOMER_RENTAL_PATH;

@Api(tags = "rentals")
@Service
@HATEOAS
@Path(CUSTOMER_RENTAL_PATH)
public class CustomerRentalResource implements CustomerRentalRestApi {

    private final CustomerRentalFacade customerRentalFacade;

    private final CustomerRentalService customerRentalService;

    private final CustomerRentalMapper mapper;


    public CustomerRentalResource(CustomerRentalFacade customerRentalFacade, CustomerRentalService customerRentalService, CustomerRentalMapper mapper) {
        this.customerRentalFacade = customerRentalFacade;
        this.customerRentalService = customerRentalService;
        this.mapper = mapper;
    }


    @Override
    public CustomerRentalInfoResponse customerRentalInfo(@ApiParam(value = "Customer token", required = true) String customerToken,
                                                         @ApiParam("Films tokens") List<String> films) {
        final Map<Boolean, List<Object>> filmsRentedByCustomerAndErrors;

        if (films.isEmpty()){
            filmsRentedByCustomerAndErrors = customerRentalFacade.obtainFilmsRentedByCustomer(customerToken);
        } else {
            filmsRentedByCustomerAndErrors = customerRentalFacade.obtainFilmsRentedByCustomer(customerToken, films,
                    (customerFilmPair, film) -> customerRentalService.rentalInfo(customerFilmPair.getKey(),
                            customerFilmPair.getValue()).orElseThrow(() -> new RentalException (
                                    "The film [" +  customerFilmPair.getValue() + "] was not rented by customer [" +  customerFilmPair.getKey() + "]"
                            )
                    ), Function.identity());

        }

        final Double finalPrice = customerRentalFacade.computeFinalPrice(filmsRentedByCustomerAndErrors, FilmRentedByCustomerModel::toPay);
        return mapper.map(filmsRentedByCustomerAndErrors, finalPrice);
    }

    @Override
    public Response rentFilms(@ApiParam(value = "Customer token", required = true) String customerToken,
                              @ApiParam(value = "List of films to rent", required = true) List<FilmRentalRequest> films) throws NotFoundException {
        final Map<Boolean, List<Object>> filmsRentedByCustomerAndErrors = customerRentalFacade.obtainFilmsRentedByCustomer(
                customerToken,
                films,
                (customerFilmPair, film) -> customerRentalService.rentFilm(customerFilmPair.getKey(), customerFilmPair.getValue(), film.daysToRent()),
                FilmRentalRequest::film);

        final Double finalPrice = customerRentalFacade.computeFinalPrice(filmsRentedByCustomerAndErrors, FilmRentedByCustomerModel::paid);
        return Response.accepted().entity(mapper.map(filmsRentedByCustomerAndErrors, finalPrice)).build();
    }

    @Override
    public Response returnFilms(@ApiParam(value = "Customer token", required = true) String customerToken,
                                @ApiParam(value = "List of films to return", required = true) List<String> films) {

        final Map<Boolean, List<Object>> filmsRentedByCustomerAndErrors = customerRentalFacade.obtainFilmsRentedByCustomer(
                customerToken,
                films,
                (customerFilmPair, filmId) -> customerRentalService.returnFilm(customerFilmPair.getKey(), customerFilmPair.getValue()),
                Function.identity());

        final Double finalPrice = customerRentalFacade.computeFinalPrice(filmsRentedByCustomerAndErrors, FilmRentedByCustomerModel::paid);
        return Response.accepted().entity(mapper.map(filmsRentedByCustomerAndErrors, finalPrice)).build();
    }


}
