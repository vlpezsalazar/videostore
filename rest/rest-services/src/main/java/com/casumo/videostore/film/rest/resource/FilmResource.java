package com.casumo.videostore.film.rest.resource;

import com.casumo.videostore.film.rest.api.FilmRestApi;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.film.rest.dto.mapper.FilmMapper;
import com.casumo.videostore.film.rest.facade.FilmFacade;
import com.casumo.videostore.rest.hateoas.HATEOAS;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Service;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import static com.casumo.videostore.film.rest.api.FilmApiPath.FILM;
import static com.casumo.videostore.film.rest.api.FilmApiPath.FILMS;
import static com.casumo.videostore.rest.configuration.JerseyConfig.VIDEOSTORE_REST;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;

@Api(tags = "films")
@Service
@HATEOAS
@Path(FILMS)
public class FilmResource implements FilmRestApi {

    private final FilmFacade filmFacade;

    private final FilmMapper mapper;

    public FilmResource(FilmFacade filmService, FilmMapper mapper) {

        this.filmFacade = filmService;
        this.mapper = mapper;
    }

    @Override
    public Response addFilm(@ApiParam(value = "Description of the film to add to the system") FilmDescriptionDto filmDto){
        final String filmId = filmFacade.createFilm(mapper.map(filmDto));
        return Response.created(UriBuilder.fromPath(VIDEOSTORE_REST).path(FilmResource.class).path(FILM).build(filmId)).build();
    }

    @Override
    public Response deleteFilm(@ApiParam(value = "Film (token) to remove") String film) throws NotFoundException {
        filmFacade.deleteFilm(film);
        return Response.status(NO_CONTENT).build();
    }

}
