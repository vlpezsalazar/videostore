package com.casumo.videostore.rental.rest.dto.mapper;

import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.model.ImmutableFilmModel;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.film.rest.api.dto.ImmutableFilmDescriptionDto;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoDto;
import com.casumo.videostore.rental.rest.api.dto.ImmutableCustomerRentalInfoDto;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.metadata.Type;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.Optional;

@Configuration
public class CustomerRentalMapperConfiguration {

    private final MapperFactory mapperFactory;

    public CustomerRentalMapperConfiguration(MapperFactory mapperFactory) {

        this.mapperFactory = mapperFactory;

        mapperFactory.registerConcreteType(FilmModel.class, ImmutableFilmModel.class);

        mapperFactory.classMap(FilmModel.class, ImmutableFilmDescriptionDto.class)
                .fieldAToB("filmName", "name")
                .register();

        mapperFactory.registerConcreteType(FilmDescriptionDto.class, ImmutableFilmDescriptionDto.class);

        mapperFactory.classMap(FilmRentedByCustomerModel.class, ImmutableCustomerRentalInfoDto.class)
                .fieldAToB("filmModel", "film")
                .byDefault()
                .register();

        mapperFactory.registerConcreteType(CustomerRentalInfoDto.class, ImmutableCustomerRentalInfoDto.class);


        mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(Date.class));
        mapperFactory.getConverterFactory().registerConverter(new CustomConverter<Optional<Date>, Date>() {
            @Override
            public Date convert(Optional<Date> source, Type<? extends Date> destinationType) {
                return source.orElse(null);
            }
        });

    }

    @Bean
    public BoundMapperFacade<FilmRentedByCustomerModel, CustomerRentalInfoDto> mapperFacadeFilmRentedByCustomerModelToCustomerRentalInfoDto(){
        return mapperFactory.getMapperFacade(FilmRentedByCustomerModel.class, CustomerRentalInfoDto.class);
    }

}
