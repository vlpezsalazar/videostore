package com.casumo.videostore.rest.hateoas;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.jaxrs.JaxRsLinkBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Component
@HATEOAS
public class RestResponseFilter implements ContainerResponseFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Context
    HttpServletRequest request;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {

        populateSpringRequestContext();
        ResourceSupport resource = new RestResponseWrapper(responseContext.getEntity());

        resource.add(
                JaxRsLinkBuilder.linkTo(resourceInfo.getResourceClass(), requestContext.getUriInfo().getPathParameters())
                        .withSelfRel());

       responseContext.setEntity(resource, null, MediaType.APPLICATION_JSON_TYPE);
    }

    private void populateSpringRequestContext() {
        ServletRequestAttributes attributes = new ServletRequestAttributes(request);
        request.setAttribute(RequestContextListener.class.getName() + ".REQUEST_ATTRIBUTES", attributes);
        LocaleContextHolder.setLocale(request.getLocale());
        RequestContextHolder.setRequestAttributes(attributes);
    }
}
