package com.casumo.videostore.film.rest.dto.mapper;

import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.model.ImmutableFilmModel;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.film.rest.api.dto.ImmutableFilmDescriptionDto;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalInfoDto;
import com.casumo.videostore.rental.rest.api.dto.ImmutableFilmRentalInfoDto;
import ma.glasnost.orika.BoundMapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.metadata.TypeBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.Optional;

@Configuration
public class FilmMapperConfiguration {

    private MapperFactory mapperFactory;

    public FilmMapperConfiguration(MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;

        mapperFactory.classMap(ImmutableFilmModel.class, ImmutableFilmDescriptionDto.class)
                .fieldMap("filmName", "name").add()
                .byDefault()
                .register();

        mapperFactory.registerConcreteType(FilmDescriptionDto.class, ImmutableFilmDescriptionDto.class);
        mapperFactory.registerConcreteType(FilmModel.class, ImmutableFilmModel.class);


        mapperFactory.classMap(FilmRentalInfoBean.class, ImmutableFilmRentalInfoDto.class)
                .byDefault()
                .register();

        mapperFactory.registerConcreteType(FilmRentalInfoDto.class, ImmutableFilmRentalInfoDto.class);


        mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(Date.class));
        mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(new TypeBuilder<Optional<Date>>(){}.build()));
    }

    @Bean
    public BoundMapperFacade<FilmModel, FilmDescriptionDto> mapperFilmModelToFilmDescriptionDto(){
        return mapperFactory.getMapperFacade(FilmModel.class, FilmDescriptionDto.class);
    }

    @Bean
    public BoundMapperFacade<FilmRentalInfoBean, FilmRentalInfoDto> mapperFacadeFilmRentalInfoBeanToFilmRentalInfoDto(){
        return mapperFactory.getMapperFacade(FilmRentalInfoBean.class, FilmRentalInfoDto.class);
    }
}
