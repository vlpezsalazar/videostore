package com.casumo.videostore.rental.test.resource;

import com.casumo.videostore.IntegrationTestPersistenceConfig;
import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoDto;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalRequest;
import com.casumo.videostore.rest.Application;
import com.casumo.videostore.rest.hateoas.RestResponseWrapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import static com.casumo.videostore.customer.rest.api.CustomerApiPath.CUSTOMER;
import static com.casumo.videostore.rental.rest.api.CustomerRentalApiPath.CUSTOMER_RENTAL;
import static com.casumo.videostore.rental.rest.api.media.CustomerRentalMediaType.CASUMO_VIDEO_STORE_FILM_RENTAL_JSON;
import static com.casumo.videostore.rest.configuration.JerseyConfig.VIDEOSTORE_REST;
import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.OK;
import static org.exparity.hamcrest.date.DateMatchers.within;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(SpringRunner.class)
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = {Application.class, IntegrationTestPersistenceConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:customers.xml")
public class CustomerRentalResourceIntegrationTest {

    static {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @LocalServerPort
    private int port;


    @Test
    public void customerRentalInfo_shouldProvideInformationAboutAllCurrentlyNotReturnedFilms_whenUserHasAny() {
        //given
        final String customerToken = "1";

        //when
        final Response response = CUSTOMER_RENTAL(target(), customerToken).get();


        //then
        assertThat(response.getStatusInfo(), is(OK));
        CustomerRentalInfoResponse films = getContent(response, new GenericType<RestResponseWrapper<CustomerRentalInfoResponse>>(){});
        assertThat(films.filmsRented(), hasSize(2));
        assertThat(films.filmsRented(), containsInAnyOrder(
                sameBeanAs(CustomerRentalInfoDto
                        .builder()
                        .film(FilmDescriptionDto.builder().name("Matrix 11").build())
                        .daysRented(1)
                        .toPay(0)
                        .paid(40)
                        .rentedOn(Date.from(Instant.parse("2017-12-13T20:11:13.00Z")))
                        .build()),
                sameBeanAs(CustomerRentalInfoDto
                        .builder()
                        .film(FilmDescriptionDto.builder().name("Matrix").build())
                        .daysRented(1)
                        .toPay(0)
                        .paid(30)
                        .rentedOn(Date.from(Instant.parse("2017-12-13T20:11:13.00Z")))
                        .build())));

        assertThat(films.errors(), empty());

    }

    @Test
    public void customerRent_shouldRentAFilm_whenTheFilmIsAvailable() {
        //given
        final String customerToken = "2";
        final List<FilmRentalRequest> films = Arrays.asList(FilmRentalRequest.of("1", 3));
        final java.util.Date rentedOn = Date.from(Instant.now());

        //when
        final Response response = CUSTOMER_RENTAL(target(), customerToken).post(Entity.entity(films, CASUMO_VIDEO_STORE_FILM_RENTAL_JSON));
        //and
        final Response customerResponse = CUSTOMER(target(), "2").get();

        //then
        assertThat(response.getStatusInfo(), is(ACCEPTED));
        CustomerRentalInfoResponse rental = getContent(response, new GenericType<RestResponseWrapper<CustomerRentalInfoResponse>>(){});
        assertThat(rental, notNullValue());
        assertThat(rental.errors(), empty());
        assertThat(rental.filmsRented(), hasItems(sameBeanAs(CustomerRentalInfoDto
                .builder()
                .film(FilmDescriptionDto.builder().name("Terminator Salvation").build())
                .daysRented(3)
                .toPay(0)
                .paid(30)
                .rentedOn(rentedOn)
                .build()).with("rentedOn", within(1, ChronoUnit.SECONDS, rentedOn))));

        //and
        assertThat(customerResponse.getStatusInfo(), is(OK));
        CustomerDescriptionResponse customer = getContent(customerResponse,  new GenericType<RestResponseWrapper<CustomerDescriptionResponse>>() {});
        assertThat(customer.earnedBonusPoints(), is(1L));
    }

    @Test
    public void customerReturn_shouldReturnAFilm_whichWasPreviouslyRented() {
        //given
        final String customerToken = "1";
        final List<String> filmsReturned = Arrays.asList("3");

        //when
        final Response response = CUSTOMER_RENTAL(target(), customerToken).method("PATCH", Entity.entity(filmsReturned, CASUMO_VIDEO_STORE_FILM_RENTAL_JSON));

        //then
        assertThat(response.getStatusInfo(), is(ACCEPTED));
        CustomerRentalInfoResponse rental = getContent(response, new GenericType<RestResponseWrapper<CustomerRentalInfoResponse>>() {
        });
        assertThat(rental.filmsRented(), hasItems(sameBeanAs(CustomerRentalInfoDto
                .builder()
                .film(FilmDescriptionDto.builder().name("Matrix").build())
                .daysRented(1)
                .toPay(0)
                .paid(30)
                .rentedOn(Date.from(Instant.parse("2017-12-13T20:11:13.00Z")))
                .build()).with("returnedOn", within(1, ChronoUnit.SECONDS, Date.from(Instant.now())))));
        assertThat(rental.errors(), empty());
    }

    public <Type> Type getContent(Response response, GenericType<RestResponseWrapper<Type>> type) {
        final RestResponseWrapper<Type> rentalRest = response.readEntity(type);
        return rentalRest.getContent();
    }

    private WebTarget target() {
        ClientConfig config = new ClientConfig();
        config.property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);
        Client client = ClientBuilder.newClient(config);
        return client.target("http://localhost:" + this.port + VIDEOSTORE_REST);
    }
}
