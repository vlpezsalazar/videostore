package com.casumo.videostore;

import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class IntegrationTestPersistenceConfig {

    @Bean
    DatabaseDataSourceConnectionFactoryBean dbUnitDatabaseConnection(@Value("${db_schema}") String schema, @Autowired DataSource dataSource){
        final DatabaseDataSourceConnectionFactoryBean databaseDataSourceConnectionFactoryBean = new DatabaseDataSourceConnectionFactoryBean(dataSource);
        databaseDataSourceConnectionFactoryBean.setSchema(schema);
        return databaseDataSourceConnectionFactoryBean;
    }
    
}
