package com.casumo.videostore.rental.rest.dto.mapper;

import com.casumo.videostore.domain.test.fixtures.FilmRentedByCustomerModelFixtures;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoDto;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalNotFoundDto;
import com.casumo.videostore.rest.configuration.MapperFactoryConfiguration;
import com.casumo.videostore.rest.test.fixtures.CustomerRentalInfoDtoFixtures;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;

public class CustomerRentalMapperTest {

    private CustomerRentalMapperConfiguration configuration = createConfiguration();

    private CustomerRentalMapper mapper = new CustomerRentalMapper(
            configuration.mapperFacadeFilmRentedByCustomerModelToCustomerRentalInfoDto()
    );

    private CustomerRentalMapperConfiguration createConfiguration() {
        MapperFactoryConfiguration mapperFactory = new MapperFactoryConfiguration();
        return new CustomerRentalMapperConfiguration(mapperFactory.mapperFactory());
    }

    @Test
    public void map_shouldTransform_FilmRentedByCustomerModel_to_FilmRentalInfoDto() throws Exception {
        //given
        FilmRentedByCustomerModel filmRentedByCustomerModel = FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengersTodayFor2DaysPaying100();

        //when
        CustomerRentalInfoDto filmRentalInfoDto = mapper.map(filmRentedByCustomerModel);

        //then
        assertThat(filmRentalInfoDto.film().name(), is(filmRentedByCustomerModel.filmModel().filmName()));
        assertThat(filmRentalInfoDto.daysRented(), is(filmRentedByCustomerModel.daysRented()));
        assertThat(filmRentalInfoDto.paid(), is(filmRentedByCustomerModel.paid()));
        assertThat(filmRentalInfoDto.toPay(), is(filmRentedByCustomerModel.toPay()));
        assertThat(filmRentalInfoDto.rentedOn(), is(filmRentedByCustomerModel.rentedOn()));
        assertThat(Optional.ofNullable(filmRentalInfoDto.returnedOn()), is(filmRentedByCustomerModel.returnedOn()));

    }

    @Test
    public void map_shouldTransform_FilmRentalRequest_to_RentFilmParamBean() throws Exception {
        //given
        final Date now = Date.from(Instant.now());
        final Map<Boolean, List<Object>> filmRentalRequest = FilmRentedByCustomerModelFixtures.generateASetOfGoodCustomerRentedAvengersFor2DaysPayingBasicPriceAndABadFilmOn(now);
        final FilmRentalNotFoundDto badFilm = FilmRentalNotFoundDto.of("BadFilm");
        final CustomerRentalInfoDto customerRentalInfoDto = CustomerRentalInfoDtoFixtures.goodCustomerRentedAvengersTodayFor2DaysPayingBasicPriceOn(now);

        //when
        CustomerRentalInfoResponse rentFilmParamBean = mapper.map(filmRentalRequest, 30d);

        //then
        assertThat(rentFilmParamBean.filmsRented(), hasItems(sameBeanAs(customerRentalInfoDto)));
        assertThat(rentFilmParamBean.errors(), hasItems(sameBeanAs(badFilm)));
        assertThat(rentFilmParamBean.totalPrice(), is(30d));

    }

}