package com.casumo.videostore.film.rest.resource;

import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.rest.api.FilmApiPath;
import com.casumo.videostore.film.rest.api.FilmRestApi;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.film.rest.dto.mapper.FilmMapper;
import com.casumo.videostore.film.rest.facade.FilmFacade;
import com.casumo.videostore.rest.test.fixtures.FilmDescriptionDtoFixtures;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;

import static com.casumo.videostore.rest.configuration.JerseyConfig.VIDEOSTORE_REST;
import static javax.ws.rs.core.HttpHeaders.LOCATION;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class FilmResourceTest {

    private FilmRestApi filmRestApi;

    @Mock
    private FilmFacade filmService;

    @Mock
    private FilmMapper filmMapper;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        filmRestApi = new FilmResource(filmService, filmMapper);
    }

    @Test
    public void addFilm_shouldDelegateToFilmService_whenFilmTokenIsValid() throws Exception {
        //given
        final String filmId = "1000L";
        final FilmModel filmToken = FilmModelFixtures.avengers();
        FilmDescriptionDto avengers = FilmDescriptionDtoFixtures.avengers();
        given(filmService.createFilm(filmToken)).willReturn(filmId);
        given(filmMapper.map(avengers)).willReturn(FilmModelFixtures.avengers());

        //when
        final Response response = filmRestApi.addFilm(avengers);

        //then
        assertThat(response.getStatusInfo(), is(CREATED));
        assertThat(response.getHeaderString(LOCATION), is(FilmApiPath.FILM(VIDEOSTORE_REST, filmId)));
    }

    @Test
    public void deleteFilm_shouldDelegateToFilmService_whenFilmTokenIsValid() throws Exception {
        //given
        final String filmToken = "filmToken";

        //when
        final Response response = filmRestApi.deleteFilm(filmToken);

        //then
        verify(filmService).deleteFilm(filmToken);
        assertThat(response.getStatusInfo(), is(NO_CONTENT));
        assertThat(response.getEntity(), nullValue());
    }
}