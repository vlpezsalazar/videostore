package com.casumo.videostore.customer.rest.dto.mapper;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;
import com.casumo.videostore.customer.rest.api.dto.CustomerDescriptionResponse;
import com.casumo.videostore.domain.test.fixtures.CustomerModelFixtures;
import com.casumo.videostore.rest.configuration.MapperFactoryConfiguration;
import com.casumo.videostore.rest.test.fixtures.CustomerCreationRequestFixtures;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CustomerMapperTest {

    private CustomerMapperConfiguration configuration = createConfiguration();

    private CustomerMapper customerMapper = new CustomerMapper(
            configuration.mapperCustomerCreationRequestToCustomerModel(),
            configuration.mapperCustomerCustomerModelToCustomerDescriptionResponse()
    );

    private CustomerMapperConfiguration createConfiguration() {
        MapperFactoryConfiguration mapperFactory = new MapperFactoryConfiguration();
        return new CustomerMapperConfiguration(mapperFactory.mapperFactory());
    }

    @Test
    public void map_shouldTransform_CustomerCreationRequest_to_CustomerParamBean() throws Exception {

        //given
        final CustomerCreationRequest customerCreationRequest = CustomerCreationRequestFixtures.goodCustomer();

        //when
        final CustomerModel customerModel = customerMapper.map(customerCreationRequest);

        //then
        assertThat(customerModel.name(), is(customerCreationRequest.name()));

    }

    @Test
    public void map_shouldTransform_CustomerModel_to_CustomerDescriptionResponse() throws Exception {
        //given
        final CustomerModel customerModel = CustomerModelFixtures.goodCustomer();

        //when
        final CustomerDescriptionResponse customerDescriptionResponse = customerMapper.map(customerModel);

        //then
        assertThat(customerModel.name(), is(customerDescriptionResponse.name()));


    }

}