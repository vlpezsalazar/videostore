package com.casumo.videostore.rest.errormapper;

import com.casumo.videostore.dto.ErrorResponse;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.hamcrest.core.Is.is;

public class ErrorMapperTest {

    private ErrorMapper errorMapper = new ErrorMapper();

    @Test
    public void toResponse_shouldBuildAnErrorResponseUsingTheExceptionMessage() throws Exception {
        //given
        final String exceptionMessage = "exception message";
        final ErrorResponse errorResponse = ErrorResponse.builder().message("message").build();
        final Exception exception = new Exception(exceptionMessage);

        //when
        final Response response = errorMapper.toResponse(exception);

        //then
        assertThat(response.getStatusInfo(), is(INTERNAL_SERVER_ERROR));
        assertThat(response.getEntity(), instanceOf(ErrorResponse.class));
        assertThat(response.getEntity(), samePropertyValuesAs(errorResponse));

    }

}