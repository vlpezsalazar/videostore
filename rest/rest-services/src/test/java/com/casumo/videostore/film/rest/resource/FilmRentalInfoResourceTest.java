package com.casumo.videostore.film.rest.resource;

import com.casumo.videostore.domain.test.fixtures.FilmRentalInfoBeanFixtures;
import com.casumo.videostore.film.bean.FilmRentalInfoBean;
import com.casumo.videostore.film.rest.api.FilmRentalInfoApi;
import com.casumo.videostore.film.rest.dto.mapper.FilmMapper;
import com.casumo.videostore.film.rest.facade.FilmFacade;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class FilmRentalInfoResourceTest {

    @Mock
    private FilmFacade filmFacade;

    @Mock
    private FilmMapper filmMapper;

    private FilmRentalInfoApi filmRestApi;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        filmRestApi = new FilmRentalInfoResource(filmFacade, filmMapper);
    }

    @Test
    public void filmRentalInfo() throws Exception {
    }


    @Test
    public void filmRentalInfo_shouldDelegateToFilmFacade_whenFilmTokenIsValid() throws Exception {
        //given
        final int daysToRent = 2;
        final String filmToken = "filmToken";
        final FilmRentalInfoBean filmRentalInfoBean = FilmRentalInfoBeanFixtures.avengersRentalFor2Days();
        given(filmFacade.filmRentalInfo(filmToken, daysToRent)).willReturn(filmRentalInfoBean);

        //when
        filmRestApi.filmRentalInfo(filmToken, daysToRent);

        //then
        verify(filmMapper).map(filmRentalInfoBean);
    }


}