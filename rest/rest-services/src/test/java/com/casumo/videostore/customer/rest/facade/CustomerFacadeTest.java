package com.casumo.videostore.customer.rest.facade;

import com.casumo.videostore.customer.dao.CustomerDao;
import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.model.ImmutableCustomerModel;
import com.casumo.videostore.domain.test.IAssertException;
import com.casumo.videostore.domain.test.fixtures.CustomerModelFixtures;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.NotFoundException;
import java.util.Optional;

import static com.casumo.videostore.domain.test.AssertException.captureException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;


public class CustomerFacadeTest {

    private CustomerFacade customerFacade;

    @Mock
    private CustomerDao customerDao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        customerFacade = new CustomerFacade(customerDao);
    }

    @Test
    public void createCustomer_shouldDelegateToCustomerDao() throws Exception {
        //given
        CustomerModel customerModel = CustomerModelFixtures.goodCustomer();
        given(customerDao.persist(customerModel)).willReturn(customerModel);

        //when
        final String customer = customerFacade.createCustomer(customerModel);

        //then
        assertThat(customer, is(String.valueOf(customerModel.id())));
    }

    @Test
    public void find_shouldDelegateToCustomerDao() throws Exception {
        //given
        final String customerToken = "100001";
        final CustomerModel customerModel = CustomerModelFixtures.goodCustomer();
        given(customerDao.findById(100001L)).willReturn(Optional.of(customerModel));

        //when
        final CustomerModel model = customerFacade.find(customerToken);

        //then
        assertThat(model, is(customerModel));
        assertThat(model, instanceOf(ImmutableCustomerModel.class));
    }

    @Test
    public void find_shouldThrowCustomerModelNotFoundException_whenDelegateToCustomerDao() throws Exception {
        //given
        final String customerToken = "100001";

        //when
        final IAssertException exceptionThrown = captureException(() -> customerFacade.find(customerToken));

        //then
        exceptionThrown.is(NotFoundException.class);
    }

    @Test
    public void deleteCustomer_shouldDelegateToCustomerDao() throws Exception {
        //given
        final String customerToken = "10001";
        final Long id = 10001L;
        given(customerDao.deleteById(id)).willReturn(1L);

        //when
        customerFacade.deleteCustomer(customerToken);

        //then
        verify(customerDao).deleteById(id);
    }

    @Test
    public void deleteCustomer_shouldThrowCustomerNotFoundException_whenCustomerTokenIsNotFound() throws Exception {
        //given
        final String customerToken = "10001";
        given(customerDao.deleteById(10001L)).willReturn(0L);

        //when
        final IAssertException exceptionThrown = captureException(() -> customerFacade.deleteCustomer(customerToken));

        //then
        exceptionThrown.is(NotFoundException.class);
    }

}