package com.casumo.videostore.film.rest.facade;

import com.casumo.videostore.domain.test.AssertException;
import com.casumo.videostore.domain.test.IAssertException;
import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.film.FilmInfoService;
import com.casumo.videostore.film.dao.FilmDao;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.model.ImmutableFilmModel;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.NotFoundException;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class FilmFacadeTest {
    private FilmFacade filmFacade;

    @Mock
    private FilmDao filmDao;

    @Mock
    private FilmInfoService filmInfoService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        filmFacade = new FilmFacade(filmDao, filmInfoService);
    }

    @Test
    public void createFilm_shouldDelegateToFilmDao() throws Exception {
        //given
        FilmModel filmModel = FilmModelFixtures.avengers();
        given(filmDao.persist(filmModel)).willReturn(filmModel);

        //when
        final String film = filmFacade.createFilm(filmModel);

        //then
        assertThat(film, Is.is(String.valueOf(filmModel.id())));
    }

    @Test
    public void find_shouldDelegateToFilmDao() throws Exception {
        //given
        final String filmToken = "100001";
        final FilmModel filmModel = FilmModelFixtures.avengers();
        given(filmDao.findById(100001L)).willReturn(Optional.of(filmModel));

        //when
        final FilmModel model = filmFacade.findFilm(filmToken);

        //then
        assertThat(model, Is.is(filmModel));
        assertThat(model, Matchers.instanceOf(ImmutableFilmModel.class));
    }

    @Test
    public void find_shouldThrowNotFoundException_whenDelegateToFilmDao() throws Exception {
        //given
        final String filmToken = "100001";

        //when
        final IAssertException exceptionThrown = AssertException.captureException(() -> filmFacade.findFilm(filmToken));

        //then
        exceptionThrown.is(NotFoundException.class);
    }

    @Test
    public void deleteFilm_shouldDelegateToFilmDao() throws Exception {
        //given
        final String filmToken = "10001";
        final Long id = 10001L;
        given(filmDao.deleteById(id)).willReturn(1L);

        //when
        filmFacade.deleteFilm(filmToken);

        //then
        verify(filmDao).deleteById(id);
    }

    @Test
    public void deleteFilm_shouldThrowFilmNotFoundException_whenFilmTokenIsNotFound() throws Exception {
        //given
        final String filmToken = "10001";
        given(filmDao.deleteById(10001L)).willReturn(0L);

        //when
        final IAssertException assertException = AssertException.captureException(() -> filmFacade.deleteFilm(filmToken));

        //then
        assertException.hasNotBeenThrown();
    }

    @Test
    public void filmRentalInfo_shouldRetrieveFilmFromFilmDaoAndDelegateToFilmInfoService() throws Exception {
        //given
        final int daysToRent = 1;
        final FilmModel avengers = FilmModelFixtures.avengers();
        final String filmToken = String.valueOf(avengers.id());
        given(filmDao.findById(avengers.id())).willReturn(Optional.of(avengers));

        //when
        filmFacade.filmRentalInfo(filmToken, daysToRent);

        //then
        verify(filmInfoService).filmRentalInfo(avengers, daysToRent);
    }

}