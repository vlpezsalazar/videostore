package com.casumo.videostore.rental.rest.facade;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.rest.facade.CustomerFacade;
import com.casumo.videostore.domain.test.fixtures.CustomerModelFixtures;
import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.domain.test.fixtures.FilmRentedByCustomerModelFixtures;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.rest.facade.FilmFacade;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.exception.RentalException;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.NotFoundException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;

public class CustomerRentalFacadeTest {

    private CustomerRentalFacade customerRentalFacade;

    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private FilmFacade filmFacade;

    @Mock
    private FilmRentedByCustomerDao filmRentedByCustomerDao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        customerRentalFacade = new CustomerRentalFacade(customerFacade, filmFacade, filmRentedByCustomerDao);
    }

    @Test
    public void obtainFilmsRentedByCustomer_shouldPopulateFilmsAndErrors_whenTheCustomerIsValidAndThereAreFilmsNotFound() throws Exception {
        //given
        final CustomerModel goodCustomer = CustomerModelFixtures.goodCustomer();
        final String customerToken = String.valueOf(goodCustomer.id());
        final String token1 = "token1",
                token2 = "token2",
                token3 = "token3";
        final FilmModel avengers = FilmModelFixtures.avengers();
        final FilmRentedByCustomerModel avengersRentedByGoodCustomer = FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengers4DaysAgoFor2DaysPayingBasicPrice();
        given(customerFacade.find(customerToken)).willReturn(goodCustomer);

        given(filmFacade.findFilm(token1)).willReturn(avengers);
        given(filmFacade.findFilm(token2)).willReturn(avengers);
        willThrow(new NotFoundException("cause")).given(filmFacade).findFilm(token3);

        //when
        final Map<Boolean, List<Object>> filmsRentedByCustomer = customerRentalFacade.obtainFilmsRentedByCustomer(customerToken,
                Arrays.asList(token1, token2, token3),
                (customerModelFilmModelPair, filmToken) -> {
                    assertThat(customerModelFilmModelPair.getKey(), is(goodCustomer));
                    assertThat(customerModelFilmModelPair.getValue(), is(avengers));
                    assertThat(filmToken, anyOf(equalTo(token1), equalTo(token2)));
                    return avengersRentedByGoodCustomer;
                },
                Function.identity());

        assertThat(filmsRentedByCustomer.size(), is(2));
        assertThat(filmsRentedByCustomer.get(true), hasItems(avengersRentedByGoodCustomer, avengersRentedByGoodCustomer));
        assertThat(filmsRentedByCustomer.get(false), hasItems("cause"));

    }

    @Test
    public void obtainFilmsRentedByCustomer_shouldPopulateFilmsAndErrors_whenTheCustomerIsValidAndfilmRentalGeneratorThrowsRentalOrNotFoundExceptionCustomer() throws Exception {
        //given
        final CustomerModel goodCustomer = CustomerModelFixtures.goodCustomer();
        final String customerToken = String.valueOf(goodCustomer.id());
        final String token1 = "token1",
                token2 = "token2",
                token3 = "token3";
        final String exceptionMessage1 = "cause1";
        final String exceptionMessage2 = "cause2";
        final FilmModel avengers = FilmModelFixtures.avengers();
        final FilmRentedByCustomerModel avengersRentedByGoodCustomer = FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengers4DaysAgoFor2DaysPayingBasicPrice();
        given(customerFacade.find(customerToken)).willReturn(goodCustomer);

        given(filmFacade.findFilm(token1)).willReturn(avengers);
        given(filmFacade.findFilm(token2)).willReturn(avengers);
        given(filmFacade.findFilm(token3)).willReturn(avengers);

        //when
        final Map<Boolean, List<Object>> filmsRentedByCustomer = customerRentalFacade.obtainFilmsRentedByCustomer(customerToken,
                Arrays.asList(token1, token2, token3),
                (customerModelFilmModelPair, filmToken) -> {
                    assertThat(customerModelFilmModelPair.getKey(), is(goodCustomer));
                    assertThat(customerModelFilmModelPair.getValue(), is(avengers));
                    if(filmToken.equals(token3)) throw new RentalException(exceptionMessage1);
                    if(filmToken.equals(token2)) throw new NotFoundException(exceptionMessage2);
                    return avengersRentedByGoodCustomer;
                },
                Function.identity());

        //then
        assertThat(filmsRentedByCustomer.size(), is(2));
        assertThat(filmsRentedByCustomer.get(true), hasItems(avengersRentedByGoodCustomer));
        assertThat(filmsRentedByCustomer.get(false), containsInAnyOrder(exceptionMessage1, exceptionMessage2));

    }

    @Test
    public void computeFinalPrice_shouldAddUpAllThe() throws Exception {
        //given
        final Map<Boolean, List<Object>> filmsRentedByCustomer = FilmRentedByCustomerModelFixtures.generateASetOfGoodCustomerRentedAvengersFor2DaysPayingBasicPriceAndABadFilmOn(Date.from(Instant.now()));

        //when
        final Double price = customerRentalFacade.computeFinalPrice(filmsRentedByCustomer, FilmRentedByCustomerModel::paid);

        //then
        assertThat(price, is(30d));
    }

}