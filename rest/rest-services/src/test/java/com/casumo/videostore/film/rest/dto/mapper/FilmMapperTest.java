package com.casumo.videostore.film.rest.dto.mapper;

import com.casumo.videostore.domain.test.fixtures.FilmModelFixtures;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.rest.api.dto.FilmDescriptionDto;
import com.casumo.videostore.rest.configuration.MapperFactoryConfiguration;
import com.casumo.videostore.rest.test.fixtures.FilmDescriptionDtoFixtures;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FilmMapperTest {

    private FilmMapperConfiguration configuration = createConfiguration();

    private FilmMapper customerMapper = new FilmMapper(
            configuration.mapperFilmModelToFilmDescriptionDto(),
            configuration.mapperFacadeFilmRentalInfoBeanToFilmRentalInfoDto());

    private FilmMapperConfiguration createConfiguration() {
        MapperFactoryConfiguration mapperFactory = new MapperFactoryConfiguration();
        return new FilmMapperConfiguration(mapperFactory.mapperFactory());
    }

    @Test
    public void map_shouldTransform_FilmDescriptionDto_to_FilmModel() throws Exception {
        //given
        FilmDescriptionDto filmDescriptionDto = FilmDescriptionDtoFixtures.avengers();

        //when
        final FilmModel filmModel = customerMapper.map(filmDescriptionDto);

        //then
        assertThat(filmModel.filmName(), is(filmDescriptionDto.name()));
    }

    @Test
    public void map_shouldTransform_FilmModel_to_FilmDescriptionDto() throws Exception {
        //given
        FilmModel filmModel = FilmModelFixtures.avengers();

        //when
        final FilmDescriptionDto filmDescriptionDto = customerMapper.map(filmModel);

        //then
        assertThat(filmModel.filmName(), is(filmDescriptionDto.name()));
    }

}