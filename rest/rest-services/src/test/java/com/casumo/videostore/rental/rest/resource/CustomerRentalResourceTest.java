package com.casumo.videostore.rental.rest.resource;

import com.casumo.videostore.customer.model.CustomerModel;
import com.casumo.videostore.customer.rest.facade.CustomerFacade;
import com.casumo.videostore.domain.test.fixtures.FilmRentedByCustomerModelFixtures;
import com.casumo.videostore.film.model.FilmModel;
import com.casumo.videostore.film.rest.facade.FilmFacade;
import com.casumo.videostore.rental.CustomerRentalService;
import com.casumo.videostore.rental.dao.FilmRentedByCustomerDao;
import com.casumo.videostore.rental.model.FilmRentedByCustomerModel;
import com.casumo.videostore.rental.exception.AlreadyRentedException;
import com.casumo.videostore.rental.exception.FilmWasNotRentedByCustomerException;
import com.casumo.videostore.rental.rest.api.CustomerRentalRestApi;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoDto;
import com.casumo.videostore.rental.rest.api.dto.CustomerRentalInfoResponse;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalNotFoundDto;
import com.casumo.videostore.rental.rest.api.dto.FilmRentalRequest;
import com.casumo.videostore.rental.rest.dto.mapper.CustomerRentalMapper;
import com.casumo.videostore.rental.rest.dto.mapper.CustomerRentalMapperConfiguration;
import com.casumo.videostore.rental.rest.facade.CustomerRentalFacade;
import com.casumo.videostore.rest.configuration.MapperFactoryConfiguration;
import com.casumo.videostore.rest.test.fixtures.CustomerRentalInfoDtoFixtures;
import com.casumo.videostore.rest.test.fixtures.FilmRentalRequestFixtures;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.shazam.shazamcrest.matcher.Matchers.sameBeanAs;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CustomerRentalResourceTest {

    private CustomerRentalRestApi customerRentalResource;

    @Mock
    private CustomerRentalService customerRentalService;

    @Mock
    private FilmRentedByCustomerDao filmRentedByCustomerDao;

    private static final FilmRentedByCustomerModel avengersRentedByGoodCustomer = FilmRentedByCustomerModelFixtures.goodCustomerRentedAvengersTodayFor2DaysPaying100();
    private static final FilmModel avengers = avengersRentedByGoodCustomer.filmModel();
    private static final CustomerModel goodCustomer = avengersRentedByGoodCustomer.customerModel();
    private static final String customerToken = String.valueOf(goodCustomer.id());
    private static final String filmToken = String.valueOf(avengers.id());

    private CustomerRentalMapperConfiguration configuration = createConfiguration();

    private CustomerRentalMapper customerRentalMapper = new CustomerRentalMapper(
            configuration.mapperFacadeFilmRentedByCustomerModelToCustomerRentalInfoDto()
    );

    private CustomerRentalMapperConfiguration createConfiguration() {
        MapperFactoryConfiguration mapperFactory = new MapperFactoryConfiguration();
        return new CustomerRentalMapperConfiguration(mapperFactory.mapperFactory());
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FilmFacade filmFacade = mock(FilmFacade.class);
        CustomerFacade customerFacade = mock(CustomerFacade.class);
        given(customerFacade.find(customerToken)).willReturn(goodCustomer);
        given(filmFacade.findFilm(filmToken)).willReturn(avengers);

        customerRentalResource = new CustomerRentalResource(
                new CustomerRentalFacade(customerFacade, filmFacade, filmRentedByCustomerDao),
                customerRentalService,
                customerRentalMapper);
    }

    @Test
    public void customerRentalInfo_shouldDelegateToFilmRentalService_whenCustomerTokenIsValid() throws Exception {

        //given
        final CustomerRentalInfoDto rental = CustomerRentalInfoDtoFixtures.goodCustomerRentedAvengersFor2DaysPaying100On(avengersRentedByGoodCustomer.rentedOn());
        final List<String> films = Collections.singletonList(filmToken);

        given(customerRentalService.rentalInfo(goodCustomer, avengers)).willReturn(Optional.of(avengersRentedByGoodCustomer));

        //when
        final CustomerRentalInfoResponse response = customerRentalResource.customerRentalInfo(CustomerRentalResourceTest.customerToken, films);

        //then
        assertThat(response.filmsRented(), hasItems(sameBeanAs(rental)));
        assertThat(response.errors(), is(empty()));
        assertThat(response.totalPrice(), is(0d));

    }

    @Test
    public void customerRentalInfo_shouldSetFilmsErrorsInResponse_whenFilmTokenIsInvalid() throws Exception {
        //given
        given(customerRentalService.rentalInfo(goodCustomer, avengers)).willReturn(Optional.empty());

        //when
        final String filmToken = String.valueOf(avengers.id());
        final CustomerRentalInfoResponse response = customerRentalResource.customerRentalInfo(customerToken, Collections.singletonList(filmToken));

        //then
        assertThat(response.errors(), hasItems(instanceOf(FilmRentalNotFoundDto.class)));
        assertThat(response.filmsRented(), is(empty()));
        assertThat(response.totalPrice(), is(0d));


    }

    @Test
    public void returnFilms_shouldDelegateToFilmRentalService_whenCustomerTokenAndFilmTokenAreValid() throws Exception {
        //given
        final List<String> films = Arrays.asList(filmToken);
        final CustomerRentalInfoDto rental = CustomerRentalInfoDtoFixtures.goodCustomerRentedAvengersFor2DaysPaying100On(avengersRentedByGoodCustomer.rentedOn());


        given(customerRentalService.returnFilm(goodCustomer, avengers)).willReturn(avengersRentedByGoodCustomer);

        //when
        final Response response = customerRentalResource.returnFilms(customerToken, films);

        //then
        assertResponse(response, instanceOf(CustomerRentalInfoResponse.class),
                hasItems(equalTo(rental)),
                is(empty()),
                is(100d),
                is(ACCEPTED));
    }

    @Test
    public void returnFilms_shouldReturnAResponseWithErrorFilm_whenFilmTokenIsInvalid() throws Exception {
        //given
        final List<String> films = Arrays.asList(filmToken);
        final FilmWasNotRentedByCustomerException exception = new FilmWasNotRentedByCustomerException(goodCustomer, avengers);

        willThrow(exception).given(customerRentalService).returnFilm(goodCustomer, avengers);

        //when
        final Response response = customerRentalResource.returnFilms(customerToken, films);

        //then
        assertResponse(response, instanceOf(CustomerRentalInfoResponse.class),
                is(empty()),
                hasItems(samePropertyValuesAs(FilmRentalNotFoundDto.of(exception.getMessage()))),
                is(0d),
                is(ACCEPTED));
    }


    @Test
    public void rentFilms_shouldDelegateToFilmRentalService_whenCustomerTokenAndFilmTokenAreValid() throws Exception {
        //given
        final FilmRentalRequest filmRentalRequest = FilmRentalRequestFixtures.rentalRequestAvengersFor2Days();
        final List<FilmRentalRequest> films = Arrays.asList(filmRentalRequest);
        final CustomerRentalInfoDto rental = CustomerRentalInfoDtoFixtures.goodCustomerRentedAvengersFor2DaysPaying100On(avengersRentedByGoodCustomer.rentedOn());
        given(customerRentalService.rentFilm(goodCustomer, avengers, filmRentalRequest.daysToRent())).willReturn(avengersRentedByGoodCustomer);

        //when
        final Response response = customerRentalResource.rentFilms(customerToken, films);

        //then
        assertResponse(response,
                instanceOf(CustomerRentalInfoResponse.class),
                hasItems(rental),
                is(empty()),
                is(100d),
                is(ACCEPTED));
    }

    @Test
    public void rentFilm_shouldReturnAResponseWithErrorFilm_whenFilmTokenIsInvalid() throws Exception {
        //given
        final FilmRentalRequest filmRentalRequest = FilmRentalRequestFixtures.rentalRequestAvengersFor2Days();
        final List<FilmRentalRequest> films = Arrays.asList(filmRentalRequest);
        final AlreadyRentedException alreadyRentedException = new AlreadyRentedException(goodCustomer, avengers);
        willThrow(alreadyRentedException).given(customerRentalService).rentFilm(goodCustomer, avengers, filmRentalRequest.daysToRent());

        //when
        final Response response = customerRentalResource.rentFilms(customerToken, films);

        //then
        verify(customerRentalService).rentFilm(goodCustomer, avengers, filmRentalRequest.daysToRent());
        assertResponse(response,
                instanceOf(CustomerRentalInfoResponse.class),
                is(empty()),
                hasItems(samePropertyValuesAs(FilmRentalNotFoundDto.of(alreadyRentedException.getMessage()))),
                is(0d),
                is(ACCEPTED));
    }

    private void assertResponse(Response response, Matcher type, Matcher entityMatcher, Matcher errorMatcher, Matcher totalPriceMatcher, Matcher statusMatcher) {
        assertThat("The entity returned in the response wasn't of the required type.", response.getEntity(), type);
        assertThat("The status returned in the response was incorrect. ", response.getStatusInfo(), statusMatcher);
        final CustomerRentalInfoResponse filmResponse = ((CustomerRentalInfoResponse) response.getEntity());
        assertThat("The total price returned in the response was incorrect", filmResponse.totalPrice(), totalPriceMatcher);
        assertThat("The filmsRented field was incorrect.", filmResponse.filmsRented(), entityMatcher);
        assertThat("The errors field was incorrect", filmResponse.errors(), errorMatcher);
    }
}