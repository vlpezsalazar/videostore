package com.casumo.videostore.acceptancetest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.HttpUrlConnectorProvider;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features")
@Configuration
public class CucumberConfiguration {

    @Bean
    public WebTarget target(){
        ClientConfig config = new ClientConfig();
        config.property(HttpUrlConnectorProvider.SET_METHOD_WORKAROUND, true);
        Client client = ClientBuilder.newClient(config);
        return client.target("http://localhost:8080"  + "/videostore/rest/v0");
    }


}