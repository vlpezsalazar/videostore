package com.casumo.videostore.acceptancetest;

import org.springframework.stereotype.Component;

@Component
public class SharedState {

    private String userToken;

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
