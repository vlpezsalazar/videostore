Feature: Inventory of films
  As a customer
  I want to know what films are available in the video store
  So I can choose one for rent

  Background:
    Given The following films are in the store: Matrix 11, Spider Man, Spider Man 2

  Scenario: customer ask what films are available
    Given I am a customer
    When I ask what films are in store
    Then I got the following films: Matrix 11, Spider Man, Spider Man 2