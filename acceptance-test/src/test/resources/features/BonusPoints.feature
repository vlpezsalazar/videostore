Feature: Bonus earned by a customer
  As a customer
  I want to keep track of my earned bonus points

  Background:
    Given I am a customer
    And The following films are in the store: Matrix 11, Spider Man, Spider Man 2

  Scenario: Bonus points are earned for renting films
    And I rent 'Matrix 11' for 1 days
    And I rent 'Spider Man' for 1 days
    When I check my bonus points
    Then My bonus points are 3

