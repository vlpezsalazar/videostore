Feature: Calculate the price of a film
  As a customer
  I want to rent a film
  So that I know how much I have to pay for it

  Background:
    Given I am a customer
    And The following films are in the store: Matrix 11, Spider Man, Spider Man 2, Out of Africa

  Scenario Outline: Rent a film for some days
    When I rent '<film>' for <days rented> days
    Then  the price for renting is <final price>
    Examples:

      | film          | days rented | final price |
      | Matrix 11     | 1           | 40          |
      | Spider Man    | 5           | 90          |
      | Spider Man 2  | 2           | 30          |
      | Out of Africa | 7           | 90          |
      | Matrix 11     | 2           | 80          |


  Scenario: Renting several films
    When I rent 'Matrix 11' for 1 days
    And I rent 'Spider Man' for 5 days
    And I rent 'Spider Man 2' for 2 days
    Then the price for renting is 110


  Scenario Outline: Returning a film on time
    And I rented a film '<film>' yesterday for <days rented> days
    When I return the film today
    Then the price for renting is 0

    Examples:

      | film          | days rented |
      | Matrix 11     | 1           |
      | Spider Man    | 3           |
      | Spider Man 2  | 3           |
      | Out of Africa | 5           |


  Scenario Outline: Returning a film late
    And I rented a film '<film>' a week ago for 3 days
    When I return the film today
    Then the price for renting is <surcharges>

    Examples:

      | film          | surcharges |
      | Matrix 11     | 160        |
      | Spider Man    | 120        |
      | Out of Africa | 60         |


  Scenario: Returning several films late
    And I rented a film 'Matrix 11' three days ago for 1 day
    And I rented a film 'Spider Man' three days ago for 1 day
    When I return the films today
    Then the price for renting is 110