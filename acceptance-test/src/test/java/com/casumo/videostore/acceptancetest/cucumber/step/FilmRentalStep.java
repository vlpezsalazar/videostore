package com.casumo.videostore.acceptancetest.cucumber.step;

import cucumber.api.PendingException;
import cucumber.api.java8.En;

public class FilmRentalStep implements En{

    public FilmRentalStep() {
        When("^I rent '([a-zA-Z0-9 ]+)' for (\\d+) days$", (film, days) -> {
            throw new PendingException();
        });
        Then("^the price for renting is (\\d+)$", (days) -> {
            throw new PendingException();
        });
        Given("^I rented a film '([a-zA-Z0-9 ]+)' ([a-zA-Z0-9 ]+) for (\\d+) day(?:s)?$", (film, when, daysRented) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        When("^I return the film(?:s)? today$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
    }
}
