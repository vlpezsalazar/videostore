package com.casumo.videostore.acceptancetest.cucumber.step;

import cucumber.api.PendingException;
import cucumber.api.java8.En;

import java.util.List;

public class InventoryStep implements En {
    public InventoryStep() {

        When("^I ask what films are in store", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^I got the following films: (.*)$", (List<String> films) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
    }
}
