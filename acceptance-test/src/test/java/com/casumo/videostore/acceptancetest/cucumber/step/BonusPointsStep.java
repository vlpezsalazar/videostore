package com.casumo.videostore.acceptancetest.cucumber.step;

import cucumber.api.PendingException;
import cucumber.api.java8.En;

public class BonusPointsStep implements En{

    public BonusPointsStep() {
        When("^I check my bonus points$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^My bonus points are (\\d+)$", (bonusPoints) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
    }
}
