package com.casumo.videostore.acceptancetest.cucumber.step;

import com.casumo.videostore.acceptancetest.SharedState;
import com.casumo.videostore.customer.rest.api.dto.CustomerCreationRequest;
import cucumber.api.PendingException;
import cucumber.api.java8.En;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.casumo.videostore.customer.rest.api.CustomerApiPath.createCustomer;

@Component
public class CommonStep implements En{


    public CommonStep(WebTarget target, SharedState state) {
        Given("^I am a customer$", () -> {
            final Response customer = createCustomer(target, CustomerCreationRequest.builder().name("Acceptance Test Customer " + RandomUtils.nextInt()).build());
            final String path = customer.getLocation().getPath();
            state.setUserToken(path.substring(path.lastIndexOf("/")+1));
        });
        Given("^The following films are in the store: (.*)$", (List<String> films) -> {
            throw new PendingException();
        });
    }
}
